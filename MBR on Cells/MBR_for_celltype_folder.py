# -*- coding: utf-8 -*-
"""
Created on Tue May  4 14:34:59 2021

@author: Till

Calculate MCD for Celltype folder and save results in each single cell folder
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.font_manager as fm
#import pandas as pd
import matplotlib as mpl
import os
import pickle

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from scipy.interpolate import interp1d
#%%
plt.rcParams['figure.figsize'] = 10,6



#%%



'''Define functions'''
def getmatfiles(path):
    #Returns all the .mat files in the given directory
    #path='Y:/till/Till/Data/2019_05_08_active_passive_075agarose_after_realignment/scan'
    filenames=np.array(os.listdir(path))                            #lists all files in dir
    filenames= np.array([i for i in filenames if ".mat" in i])      #regard only .mat files
    return(filenames)

def loadData(path,number):
    import scipy.io as sio
    filenames=getmatfiles(path)
    emptyarray=np.array([])
    filepath=path+'/'+filenames[number]
    content=sio.loadmat(filepath)
    for k,v in content.items():
        if (isinstance(v,type(emptyarray)) and k!='None') :
            exec('%s=content[\'%s\']'%(k,k))                        #auto asign variable from matfile
    #print(filenames[number])
    del content
    return locals()

'''Calculate mcd  for a given tau,dmin and maxlagtime'''

def getMCD(time,pos,tau,dmin,maxtime):

    L=len(time)
    N=L
    t=time[:N]
    p=pos[:N]
    p-=np.mean(p)
    dt=t[1]

    

    
    
    
    '''Now calculate MCD'''
    tau=int(tau/dt)

    lagtime=np.linspace(0,maxtime,(500+1))
    lagtime=[tau*dt]+lagtime.tolist()
    lagtime=lagtime/dt
    lagtime=lagtime.astype(int)
    mcd=[]
    
    for i in range(tau,len(p)-int(maxtime/dt)):
        d=-p[i+tau]+p[i]
        if abs(d)>dmin:
            
            mcd_temp=(p[i+lagtime]-p[i])/d
            mcd.append(mcd_temp)
    NN=len(mcd)    
    mcd=np.asarray(mcd)
    mcd=np.mean(mcd,0)
    
    '''If mcd is positive: trjectory is going to the opposite direction from where it was at -tau'''
    # plt.figure('MCD')
    # plt.plot(lagtime*dt,mcd,label=r'$\tau$ = '+np.str(tau*dt))
    # plt.xlabel('Lagtime [s]')
    # plt.ylabel(r'$<x(t)>_{x_0,\tau,d}/x_0$')
    # plt.tight_layout()
    # plt.legend()
    return([lagtime[1:]*dt,-mcd[1:],NN])

def denoise(x,time,peaks):
    
    
    '''Denoise frequencies of 50 and 80Hz'''
    fourier=np.fft.fft(x)
    N=len(time)
    time=np.linspace(1/rate,N/rate,N)
    freq = np.fft.fftfreq(time.size,time[0])
    
    # plt.figure()
    # plt.loglog()
    # plt.plot(freq,np.abs(fourier*time[0]))
    
    # plt.plot(freq,fourier.real)
    # plt.plot(freq,fourier.imag)
    
    
    
    area=15
    #remove peaks
    for p in peaks:
        freq_index=np.where(freq==p)

        fourier[int(freq_index[0])-area:int(freq_index[0])+area]=fourier[int(freq_index[0])+area+1]

        freq_index=np.where(freq==-p)
        fourier[int(freq_index[0])-area:int(freq_index[0])+area]=fourier[int(freq_index[0])+area+1]    

    
    
    
    # plt.figure()
    # plt.loglog()
    # plt.plot(freq,np.abs(fourier*time[0]))
    
    
    x_new=np.fft.ifft(fourier)
    
#    plt.figure()
#    plt.plot(time,x)
#    plt.plot(time,x_new)
    
    fourier=np.fft.fft(x_new)
    freq = np.fft.fftfreq(time.size,time[0])
    
    # plt.figure()
    # plt.loglog()
    # plt.plot(freq,np.abs(fourier*time[0]))
    return (x_new.real)

def correct_drift(x):
    from scipy.ndimage import gaussian_filter1d
    # plt.figure()
    # plt.plot(time,x)
    # plt.show()
    
    x_new=gaussian_filter1d(x, 3000)
    
    # plt.plot(time,x_new)
    
    x_=x-x_new
    # plt.figure()
    # plt.plot(time,x_)
    print('Drift corrected')
    return(x_)

def laplace_trafo(time,y,s_array):
    from scipy.interpolate import interp1d
    import scipy.integrate as integrate
    
    #Interpolate data
    f=interp1d(time,y)
    #Define function to integrate
    def integrand (time,s,f):
        
        return f(time)*np.exp(-s*time)
        
    #REsult array
    laplace=[]
    #Do the integration
    for s in s_array:
        a,b = integrate.quad(integrand,0,time[-1],args=(s,f))
        laplace.append(a)
        
    return(laplace)

def get_msd(y,rate):
    lagtimes=np.logspace(0,5,10)
    MSD_array=[]
    for n in lagtimes:
        n=int(n)
        diff = y[n:]-y[:-(n)] #this calculates r(t + dt) - r(t)
        diff_sq = diff**2
        MSD = np.mean(diff_sq)
        MSD_array.append(MSD)
   
    return(lagtimes/rate,MSD_array)   

def getMCD2(t,p,tau,dmin,maxtime):

    
    p-=np.mean(p)
    dt=t[0]
    
    
    
    
    
    
    '''Now calculate MCD'''
    tau=int(-tau/dt) #tau in array increments
    
    lagtime=np.linspace(0,maxtime-dt,(500+1))
    lagtime=lagtime/dt
    lagtime=lagtime.astype(int)
    #Get indices of p_array that fulfill condition
    d_array=p[tau:-int(maxtime/dt)]-p[:-tau-int(maxtime/dt)]
    to_analyse=np.abs(d_array) > dmin
    #start analzsis after some time and until some time
    to_analyse[:1000]=False
    to_analyse[-1000:]=False
    mcd=[]
    for lag in lagtime:
        a=p[tau:-int(maxtime/dt)]
        b=p[tau+lag:-int(maxtime/dt)+lag]
        dist_array=a[to_analyse]-b[to_analyse]
        mcd.append(np.mean(dist_array/d_array[to_analyse]))
        
    return([lagtime*dt,mcd,sum(to_analyse)])
    
def generatePSD(N,rate,data):
    #Generates the PSD for N samples measured with the frequency rate
    xdft=np.fft.fft(data)
    xdft=np.abs(xdft*np.conjugate(xdft))
    xdft=xdft/(N*rate)
    xdft.resize(int(N/2))
    return(xdft)

def fitpsd(psdx_ave,freq,fstart,funtil,p1,fc):
    #Fits the measured psd with a lorentzian function with the start parameters p1 and fc. The function returns the fitted lorentzian just as the fit parametrs

    
    #Start values
    #fstart=10
    #funtil=8000
    
    #Fit
    
    #Start Parameter
    #p1=10**(-0)
    #p2=40
    p2=fc
    
    parameter=[p1,p2]
    lorentz=lambda param,xx: param[0] / (xx**2 + param[1]**2)
    #plt.loglog(freq,lorentz(parameter,freq))
    
    fitx_error= lambda param: np.sum((np.log(psdx_ave[fstart:funtil]))-np.log(lorentz(param,freq[fstart:funtil])))**2
    
    a_fitx=sp.optimize.fmin(fitx_error,parameter)
    Pxx_out=lorentz(a_fitx,freq)
    return(Pxx_out,a_fitx)

def flip(pos,fliptimes):
    for flip in fliptimes:
        index=np.argmin(np.abs(time-flip))
        pos[index:]=2*np.mean(pos[index-10:index])-pos[index:]
    return pos

def timeshift(vars,time):
    trappos=trappos=vars['trappos'][0]
    trap_time=np.linspace(0,len(trappos)/50000,len(trappos))
    #Interpolate trappos
    interp_trappos=interp1d(trap_time, trappos)
    trappos_new=interp_trappos(time)
    
    fb_laser=vars['fb_mirror_y'][0]
    
    
    # plt.figure()
    # plt.plot(time,trappos_new)
    # plt.plot(time,fb_laser-np.mean(fb_laser,0))
    from scipy import signal
    def lag_finder(y1, y2, sr):
        n = len(y1)
    
        corr = signal.correlate(y2, y1, mode='same') 
    
        delay_arr = np.linspace(-0.5*n/sr, 0.5*n/sr, n)
        delay = delay_arr[np.argmax(corr)]
        # print('y2 is ' + str(delay) + ' behind y1')
        return(delay)
        # plt.figure()
        # plt.plot(delay_arr, corr)
        # plt.title('Lag: ' + str(np.round(delay, 3)) + ' s')
        # plt.xlabel('Lag')
        # plt.ylabel('Correlation coeff')
        # plt.show()
        
    return(lag_finder((fb_laser-np.mean(fb_laser,0))/np.max(fb_laser),trappos_new/np.max(trappos_new),rate))
#%%
    
root = tk.Tk()
root.withdraw()

main_path = filedialog.askdirectory()
os.chdir(main_path)

import scipy.io as sio

#%% Iterate through all folders
foldernames=[name for name in os.listdir(".") if os.path.isdir(name)]
for folder in foldernames:
    folderpath=main_path+r'/'+folder
    os.chdir(folderpath)
    experimentnames=[name for name in os.listdir(".") if os.path.isdir(name)]
    for experiment in experimentnames:
        experimentpath=folderpath+r'/'+experiment
        os.chdir(experimentpath)
        print(experimentpath)
        files=getmatfiles(experimentpath+r'/passive')
        
        
        D_calculated=[]
        All_mcd=[]
        for file in files:
            print(file)
            vars=sio.loadmat(experimentpath+r'/passive/'+file)
            
            denoising=True
            drift_correct=False
            flipping=False
            rescale=False
            maxtime=2 #0.2 gylcerol  0.001 water      0.05 cell
            tau=-0.0006 #-0.01 glycerol -0.0001 water    0.0001 cell
            dmin=0.001 # 0.001 glycerol 0.01 water       0.001 cell
            
        
            rate=65536
            constants=vars['constants'][0]
            betax=np.abs(constants[2])
            betay=np.abs(constants[3])
            
        
            
            x=vars['pos_x'][0]/betax 
            y=vars['pos_y'][0]/betay
            sumxy=vars['pos_xy'][0]
            N=np.size(y)
            
            trappos=vars['fb_mirror_y'][0]*1.45
    
   
            x=x-np.mean(x)
            y=y-np.mean(y)
            y=y/sumxy   
            
        
            if rescale:
                y=y*1.5
            
            time=np.linspace(1/rate,N/rate,N)
        #    plt.figure('Positions')
        #    plt.plot(time,y)
        #    plt.xlabel('Time [s]')
        #    plt.ylabel('Position [um]')
              
            
            if flipping:
                fliptimes=vars['flip_times']
                shift=timeshift(vars,time)
                fliptimes=fliptimes-shift
                fliptimes=fliptimes[fliptimes>0]
                if len(fliptimes) > 0:
                    y=flip(y,fliptimes)
                    trappos=flip(trappos,fliptimes)
            
            
            forcey=vars['forcey'][0]
            forcex=vars['forcex'][0]
            forcey=(forcey-np.mean(forcey))
            forcey/=np.max(forcey)
            

        
        
            # plt.figure()
            # plt.plot(time,y/np.max(y))
            # o_pos=vars['trappos_original'][0]
            # plt.plot(np.linspace(1/50000,len(o_pos)/50000,len(o_pos)),o_pos/np.max(o_pos))
        
                
            if denoising==True:
                #y=denoise(y,time,[13,68,204,271,341])
                y=denoise(y,time,[81,162,176,191,243,10399,20797,31200,30709])
                #x=denoise(x,time,[50,80,160,162,175,242])
                print('denoised')

                
                       
            [lagtimes,temp_mcd,NN]=getMCD2(time, y, tau, dmin, maxtime)
            
            #Calculate D
            x0=vars['fb_mirror_y'][0]*1.45
            x0=x0-np.mean(x0[:10])
            
            
            if flipping:
                fliptimes=vars['flip_times']
                shift=timeshift(vars,time)
                fliptimes=fliptimes-shift
                if len(fliptimes) > 0:
                    x0=flip(x0,fliptimes[0])
                    
            time_index=int(0.1*rate)
            lag=np.linspace(1,time_index,100)
            MSD_array=[]
            for n in lag:
                n=int(n)
                diff = x0[n:]*10**(-6)-x0[:-n]*10**(-6)
                #this calculates r(t + dt) - r(t)
                diff_sq = diff**2
                MSD = np.mean(diff_sq)
                MSD_array.append(MSD)
               
        
            
            D_calculated.append(MSD_array[-1]/(2*0.1))
            
            
            print(NN/len(x))
            
        
            
            All_mcd.append(temp_mcd)
        All_results=[lagtimes,All_mcd,file,np.nanmean(D_calculated)]
        with open(experimentpath+'/results_tau0p'+str(tau)[3:]+'.pkl', 'wb') as f:
            pickle.dump(All_results, f)
        with open(experimentpath+'/results_1s.pkl', 'wb') as f:
                pickle.dump(All_results, f)
        plt.figure('Final MCD')
        plt.plot(All_results[0],np.nanmean(All_results[1],0),label=str(All_results[3]))
        #plt.fill_between(All_results[0], np.nanmean(All_results[1],0)-np.nanstd(All_results[1],0), np.nanmean(All_results[1],0)+np.nanstd(All_results[1],0),alpha=0.2)
        plt.xlabel('Lagtime [s]')
        plt.ylabel(r'$<x(t)>_{x_0,\tau,d}/x_0$')
        plt.legend()
        plt.savefig(experimentpath+r'/final_mcd.png')
        plt.close()
        
        

