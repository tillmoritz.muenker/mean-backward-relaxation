# -*- coding: utf-8 -*-
"""
Created on Tue May  4 19:08:21 2021

@author: Till

Combine mcd and effective energy for all celltypes
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.font_manager as fm
#import pandas as pd
import matplotlib as mpl
import os
import pickle

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from matplotlib.lines import Line2D

import seaborn as sns
import pandas as pd
#%%

plt.rcParams['figure.figsize'] = 4,4
plt.style.use(['default'])
#%%



'''Define functions'''
def getmatfiles(path):
    #Returns all the .mat files in the given directory
    #path='Y:/till/Till/Data/2019_05_08_active_passive_075agarose_after_realignment/scan'
    filenames=np.array(os.listdir(path))                            #lists all files in dir
    filenames= np.array([i for i in filenames if ".mat" in i])      #regard only .mat files
    return(filenames)

def loadData(path,number):
    import scipy.io as sio
    filenames=getmatfiles(path)
    emptyarray=np.array([])
    filepath=path+'/'+filenames[number]
    content=sio.loadmat(filepath)
    for k,v in content.items():
        if (isinstance(v,type(emptyarray)) and k!='None') :
            exec('%s=content[\'%s\']'%(k,k))                        #auto asign variable from matfile
    #print(filenames[number])
    del content
    return locals()

def fitMCD(time,mcd,p0):
    fitfun =lambda time,amp,timescale: amp-amp*np.exp(-1*timescale*time)
    
    popt, pcov = curve_fit(fitfun, time, mcd,p0=p0,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)
fitfun =lambda time,amp,timescale: amp-amp*np.exp(-1*timescale*time)

def fitEff(freq,eff,p0):
    fitfun =lambda freq,E0: E0*freq**(-1.)+1
    
    popt, pcov = curve_fit(fitfun, freq, eff,p0=p0,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)


#%%
    
root = tk.Tk()
root.withdraw()

main_path = filedialog.askdirectory()
os.chdir(main_path)

fig_path=main_path

import scipy.io as sio

MCD_list=[]
cell_list=[]
Eff_list=[]

lagtime_list=[]
full_mcd_list=[]

freq_list=[]
full_eff_list=[]

colors=sns.color_palette()
cellstoplot=['Stemcell','C2C12','CT26','Drugged']

#tau='0p01'
tau='0p0006'


#%%
celltypes=[name for name in os.listdir(".") if os.path.isdir(name)]
for celltype in celltypes:
    full_mcd_list_=[]
    celltypepath=main_path+r'/'+celltype
    os.chdir(celltypepath)
    folders=[name for name in os.listdir(".") if os.path.isdir(name)]
    for folder in folders:
        print(folder)
        folderpath=celltypepath+r'/'+folder
        os.chdir(folderpath)
        experiments=[name for name in os.listdir(".") if os.path.isdir(name)]
        for experiment in experiments:
            experimentpath=folderpath+r'/'+experiment
            os.chdir(experimentpath)
            
            #Load MCD data
            with open(experimentpath+'/results_tau'+tau+'.pkl', 'rb') as f:
                [lagtimes,All_mcd,file,D] = pickle.load(f)  
                
            mcd=np.nanmean(np.asarray(All_mcd),0)
            
            '''Use fit to determine MBR'''
            #temp_fit=fitMCD(lagtimes,mcd,[0.5,2000])
            #MCD_list.append(temp_fit[0])
            '''Use value at time to determine MBR'''
            MCD_list.append(mcd[np.argmin(abs(lagtimes-1))])            
            
            cell_list.append(celltype)
            
            #Load effective energies
            with open(experimentpath+"/combined_new.pkl", 'rb') as f:
                data = pickle.load(f)  
            Eff=data['Eff']
            freq=data['freq']
            temp_fit=fitEff(freq,Eff,[2]) #this
            #Eff0Hz=Eff[np.where(freq==1)]
            Eff_list.append(temp_fit[0])    #this
            #Eff_list.append(Eff0Hz[0])
            
            lagtime_list.append(lagtimes)
            full_mcd_list_.append(mcd)
            full_mcd_list.append(mcd)
            
            freq_list.append(freq)
            full_eff_list.append(Eff)
            
            
            
            
            
    All_results=[lagtimes,np.mean(full_mcd_list_,0),sp.stats.sem(full_mcd_list_,0)]
    with open(celltypepath+'/results_tau'+tau+'.pkl', 'wb') as f:
        pickle.dump(All_results, f)

        
#%% 
from scipy.optimize import curve_fit
ii=np.asarray(cell_list)
plt.figure('MCD vs Eff')
plt.title('MCD vs Eff')
for i in celltypes:           
    plt.plot(np.asarray(MCD_list)[ii==i],np.asarray(Eff_list)[ii==i],'.',label=i)

plt.xlabel('MCD')
plt.ylabel('Effective energy')
plt.legend()
plt.show()

medMCD=[]
medEff=[]
meanEff=[]
plt.rcParams['figure.figsize'] = 4,4

plt.figure('MBR vs Eff median')
plt.title(r'MBR and Median $E_{\rm eff}$')
j=0
for i in celltypes:           
    #plt.plot(np.median(np.asarray(MCD_list)[ii==i],0),np.median(np.asarray(Eff_list)[ii==i],0),'.',label=i)
    medMCD.append(np.mean(np.asarray(MCD_list)[ii==i],0))
    medEff.append(np.median(np.asarray(Eff_list)[ii==i],0))
    meanEff.append(np.mean(np.asarray(Eff_list)[ii==i],0))
    plt.errorbar(np.mean(np.asarray(MCD_list)[ii==i],0), np.median(np.asarray(Eff_list)[ii==i],0), xerr=stats.sem(np.asarray(MCD_list)[ii==i],0),yerr=stats.sem(np.asarray(Eff_list)[ii==i],0),label=i,color=colors[j])
    j=j+1
plt.xlabel(r'MBR$(t \to \infty)$')
plt.ylabel('Effective Energy Amplitude $E_0$ [$k_BT$]')

plt.xlim([-.8,0.6])
plt.ylim([-1,35])
medMCD=np.asarray(medMCD)
medEff=np.asarray(medEff)
lin = lambda x,a,b : a*x+b 
popt, pcov = curve_fit(lin, np.append(medMCD,0.5), np.append(medEff,0))

MCDfit=popt
plt.plot(np.linspace(-1,1,10),lin(np.linspace(-1,1,10),*popt),color='black',linestyle='-',label='linear fit')
plt.hlines(0, 1, -1,linestyle='--',color='black',alpha=0.5)
plt.vlines(0.5, -10, 50,linestyle='--',color='black',alpha=0.5)
#plt.legend()
plt.tight_layout()
plt.show()
plt.xlim([np.min(medMCD)-0.05,0.505])
plt.text(np.min(medMCD),np.min(medEff),'f(x)='+"{:.2f}".format(popt[0])+'x+'+"{:.2f}".format(popt[1]))
filename='MBR_vs_Eff_median'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 



#%%
# for i in celltypes:  
#     plt.figure('MBR vs Eff '+i)         
#     plt.plot(np.asarray(MCD_list)[ii==i],np.asarray(Eff_list)[ii==i],'.',label=i)


# for i in celltypes:  
#     plt.figure('Effs '+i)  
#     plt.title('Effs '+i)       
#     plt.semilogx(np.asarray(freq_list)[ii==i],np.asarray(full_eff_list)[ii==i],'b.',label=i)

# #%%
# for i in celltypes:  
#     plt.figure('MCDs '+i) 
#     plt.title('MCDs '+i)       
#     plt.plot(np.asarray(lagtime_list)[ii==i],np.asarray(full_mcd_list)[ii==i],'b.',label=i)
#     plt.plot(np.mean(np.asarray(lagtime_list)[ii==i],0),np.median(np.asarray(full_mcd_list)[ii==i],0),'r-')

            
# plt.show() 

# #%%
# for i in celltypes:  
#     plt.figure('MCDs') 
#     plt.title('MCDs')       
#     #plt.plot(np.asarray(lagtime_list)[ii==i],np.asarray(full_mcd_list)[ii==i],'b.',label=i)
#     plt.plot(np.mean(np.asarray(lagtime_list)[ii==i],0),np.mean(np.asarray(full_mcd_list)[ii==i],0),label=i)
# plt.legend()
# plt.show() 
# #%%
# for i in celltypes:  
#     plt.figure('MCDs median') 
#     plt.title('MCDs')       
#     #plt.plot(np.asarray(lagtime_list)[ii==i],np.asarray(full_mcd_list)[ii==i],'b.',label=i)
#     plt.plot(np.mean(np.asarray(lagtime_list)[ii==i],0),np.median(np.asarray(full_mcd_list)[ii==i],0),label=i)
# plt.legend()
# plt.show() 

#%%
'''Use Fit value of MBR vs Eff to extract X'' and compare it to measured X'''
j=0
T=310 # K
kb=1.38*10**(-23) #kg*m^2 / (s^2 *K)
plt.figure('Response compared')
plt.xlabel(r'$X^{\prime\prime}(1Hz)[\mu m/ pN]$ from MBR')
plt.ylabel(r'$X^{\prime\prime}(1Hz)[\mu m / pN]$ from active rheology')
res_mcd=[]
res_active=[]
E0_fromMCD=[]
for i in celltypes:
    with open(main_path+'/'+i+'/all_results.pkl', 'rb') as f:
        data = pickle.load(f)
        
        temp_psd=data['medians']['psd median'][0][0]
        temp_freq=data['medians']['freq']
        temp_ResImag=data['medians']['Res imag median'][0][0]
        tem_Eff=data['medians']['E median'][0][0]
        res_from_mcd=np.pi*temp_psd/lin(medMCD[j],*popt)/(kb*T)*10**-18
        E0_fromMCD.append(lin(medMCD[j],*popt))
        #res_from_mcd=np.pi*temp_psd/medEff[j]/(kb*T)*10**-18
        res_mcd.append(res_from_mcd)
        res_active.append(temp_ResImag)
        plt.plot(res_from_mcd,temp_ResImag,'o',label=i,color=colors[j])
        plt.plot([0,0.02],[0,0.02],'k--',alpha=0.5,color=colors[j])
        
        j=j+1
        

plt.legend()
plt.tight_layout()
plt.show()

#%%
''' Show Eff fit with power law'''
'''Use this fixed power law but with amplitde gotten from MBR to determine X"'''
''' Use Cramers Kroning to determine X and from this G"'''

plt.rcParams['figure.figsize'] = 4,4
plt.style.use(['default'])
# define fit function
def fitEff(freq,eff,p0):
    fitfun =lambda freq,E0: E0*freq**(-1.)+1
    
    popt, pcov = curve_fit(fitfun, freq, eff,p0=p0,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)
fitfun =lambda freq,E0: E0*freq**(-1.)+1
C=[]
E0=[]
freq=[]
j=0
Eff_fit=[]




G_real_deviation={}
G_imag_deviation={}
Response_deviation={}



for i in celltypes:
    with open(main_path+'/'+i+'/all_results.pkl', 'rb') as f:
        data = pickle.load(f)
        
        temp_psd=data['medians']['psd median'][0]
        temp_freq=data['medians']['freq']
        temp_ResImag=data['medians']['Res imag median']
        temp_ResReal=data['medians']['Res real median']
        temp_Eff=data['medians']['E median']

        
        
        '''Interpolate PSD to longer frequencies with f^-1'''
        def get_psd(freq,short_psd):
            A=short_psd[0]
            return (A*freq**(-2))
        
        plt.figure('psd interpolated')
        loooong_freq=np.linspace(1,2**(15),2**16)
        
        plt.loglog()
        plt.plot(temp_freq,temp_psd,color=colors[j])
        plt.plot(loooong_freq,get_psd(loooong_freq,temp_psd),'--',color=colors[j])          

        #temp_freq=loooong_freq
        #temp_psd=get_psd(loooong_freq,temp_psd)
        
        
        C.append(temp_psd)
        temp_fit=fitEff(temp_freq, temp_Eff[0], [temp_Eff[0][0]])
        Eff_fit.append(temp_fit)
        E0.append(medEff[j])
        
        plt.figure('Eff fitted',figsize=(4,4))
        plt.title(r'$E_{\rm eff}$ fitted')
        plt.semilogx()
        plt.errorbar(temp_freq,temp_Eff[0],temp_Eff[1],linestyle='',marker='o',capsize=5,color=colors[j],label=str(i))
        #plt.plot(temp_freq, temp_Eff[0],'o',label=str(i),color=colors[j])
        plt.plot(temp_freq,fitfun(temp_freq,*temp_fit),color=colors[j])
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'Median Effective Energy [$k_BT$]')
        plt.legend()
        plt.tight_layout()
        filename='Eff_fitted'+tau
        plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
        plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 
        
        
        plt.rcParams['figure.figsize'] = 2.5,2
        plt.figure('Eff loglog inset')
        plt.loglog()
        plt.plot(temp_freq, temp_Eff[0]-1,'o',label=str(i),color=colors[j])
        plt.plot(temp_freq,fitfun(temp_freq,*temp_fit)-1,color=colors[j])
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'Effective Energy [$k_BT$]')
        plt.tight_layout()
        filename='Eff_loglog inset'+tau
        plt.xlim([1,100])
        plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
        plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 
        
        
        
        plt.rcParams['figure.figsize'] = 4,4
        plt.figure('Eff experimental vs Eff from MBR with powerlaw exponent -1')
        plt.title('Eff experimental vs Eff from MBR with powerlaw exponent -1')
        plt.semilogx()
        plt.plot(temp_freq, temp_Eff[0],'o',label=str(i),color=colors[j])
        plt.plot(temp_freq, fitfun(temp_freq,E0_fromMCD[j]),'--',color=colors[j])
        plt.semilogx()
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'Effective Energy [$k_BT$]')
        #plt.legend()
        plt.tight_layout()
        filename=r'/Eff_from_MBR'+tau
        plt.savefig(main_path+filename+'.png',format='png',bbox_inches='tight')
        plt.savefig(main_path+filename+'.pdf',format='pdf',bbox_inches='tight')

        plt.rcParams['figure.figsize'] = 4,2.5
        #Get X" the dissipative part of response function
        temp_resp_imag=np.pi*temp_freq*temp_psd/(fitfun(temp_freq,E0_fromMCD[j]))/(kb*T)*10**-18
        
        

        plt.figure('Response function from ap and MBR')
        plt.title(r'Response function $X^{\prime\prime}(f)$')
        plt.loglog()
        Response_deviation[i]=np.abs(1-temp_resp_imag/temp_ResImag[0])
        if i in cellstoplot:
            plt.errorbar(temp_freq,temp_ResImag[0],temp_ResImag[1],linestyle='',marker='o',capsize=5,label=str(i),color=colors[j])
            #plt.plot(temp_freq,temp_ResImag[0],'o',label=str(i),color=colors[j])
            plt.plot(temp_freq,temp_resp_imag,'--',color=colors[j])
        #plt.legend()
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'$X^{\prime\prime}(f)$')
        plt.tight_layout()
        filename='/Response_from_MBR'+tau
        plt.savefig(main_path+filename+'.png',format='png',bbox_inches='tight')
        plt.savefig(main_path+filename+'.pdf',format='pdf',bbox_inches='tight')
        
        #Now get Complex Response function X* vai Kramers Kroning
        def log_interp1d(xx, yy, kind='linear'):
            logx = np.log10(xx)
            logy = np.log10(yy)
            lin_interp = sp.interpolate.interp1d(logx, logy, kind=kind)
            log_interp = lambda zz: np.power(10.0, lin_interp(np.log10(zz)))
            return log_interp
        
        long_freq=np.linspace(1,1024,10000)
        #long_freq=np.linspace(temp_freq[0],temp_freq[-1],2**14)
        
        
        xf = log_interp1d(temp_freq, temp_resp_imag)
        resp_imag_interp=-xf(long_freq)
        
        plt.figure('Interpolate Ximag')
        plt.title('Interpolate Ximag')
        plt.loglog()
        if i in cellstoplot:
            plt.plot(temp_freq,temp_ResImag[0],'o',label=str(i),color=colors[j])
            plt.plot(long_freq,resp_imag_interp,'--',color=colors[j])
        #plt.legend()
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'$X^{\prime\prime}(f)$')
        plt.tight_layout()
        filename='/Response_from_MBR_interpolated'+tau
        plt.savefig(main_path+filename+'.png',format='png',bbox_inches='tight')
        plt.savefig(main_path+filename+'.pdf',format='pdf',bbox_inches='tight')
        
        
        
        '''Use fit to retrive G'''
        def fitXimag(freq,Ximag,p):
            A=p[0]
            alpha=p[1]
            B=p[2]
            beta=p[3]
            fitfun =lambda freq,A,alpha,B,beta: -1/(6*np.pi*0.5*10**-6)*(A*(2*np.pi*freq)**alpha*np.sin(np.pi*alpha/2)+B*(2*np.pi*freq)**beta*np.sin(np.pi*beta/2))/(A**2*(2*np.pi*freq)**(2*alpha)+B**2*(2*np.pi*freq)**(2*beta)+2*A*B*(2*np.pi*freq)**(alpha+beta)*np.cos(np.pi/2*(alpha-beta)))
            popt, pcov = curve_fit(fitfun, freq, Ximag,p0=p,maxfev=5000,bounds=([0,0,0,0],[np.infty,1,np.infty,1]))
            return(popt)
        
        fitfun2 =lambda freq,A,alpha,B,beta: -1/(6*np.pi*0.5*10**-6)*(A*(2*np.pi*freq)**alpha*np.sin(np.pi*alpha/2)+B*(2*np.pi*freq)**beta*np.sin(np.pi*beta/2))/(A**2*(2*np.pi*freq)**(2*alpha)+B**2*(2*np.pi*freq)**(2*beta)+2*A*B*(2*np.pi*freq)**(alpha+beta)*np.cos(np.pi/2*(alpha-beta)))        
        p0=[20,0.25,0.5,0.7]
        popt= fitXimag(long_freq,resp_imag_interp,p0)
        

        
        '''Now get G from this fit'''
        A=popt[0]
        alpha=popt[1]
        B=popt[2]
        beta=popt[3]
        G_from_fit=A*(1j*2*np.pi*long_freq)**(alpha)+B*(1j*2*np.pi*long_freq)**(beta)
        G_from_fit_temp_freq=A*(1j*2*np.pi*temp_freq)**(alpha)+B*(1j*2*np.pi*temp_freq)**(beta)
        
        plt.rcParams['figure.figsize'] = 4,2.5
        plt.figure('Greal from Ximag fit')
        plt.title(r'Storage Modulus $G^\prime (f)$')
        plt.loglog()
        plt.xlabel('Frequency [Hz]')
        plt.ylabel(r'$G^\prime(f) [Pa]$')
        G_real_deviation[i]=np.abs(1-G_from_fit_temp_freq.real*10**(-6)/data['medians']['G real median'][0])
        if i in cellstoplot:
            plt.errorbar(temp_freq,data['medians']['G real median'][0],data['medians']['G real median'][1],linestyle='',marker='o',color=colors[j],capsize=5)
            #plt.plot(temp_freq,data['medians']['G real median'][0],'o',color=colors[j])
            plt.plot(long_freq,G_from_fit.real*10**(-6),'--',color=colors[j])
        plt.tight_layout()
        plt.savefig(main_path+'/Greal_from_MBR'+tau+'.png',format='png',bbox_inches='tight')
        plt.savefig(main_path+'/Greal_from_MBR'+tau+'.pdf',format='pdf',bbox_inches='tight')
        
        
        plt.figure('Gimag from Ximag fit')
        plt.title(r'Loss Modulus $G^{\prime\prime}(f)$')
        plt.loglog()
        G_imag_deviation[i]=np.abs(1-G_from_fit_temp_freq.imag*10**(-6)/data['medians']['G real median'][0])
        if i in cellstoplot:
            plt.errorbar(temp_freq,data['medians']['G imag median'][0],data['medians']['G imag median'][1],linestyle='',marker='o',color=colors[j],capsize=5)
            #plt.plot(temp_freq,data['medians']['G imag median'][0],'o',color=colors[j])
            plt.plot(long_freq,G_from_fit.imag*10**(-6),'--',color=colors[j])
            plt.xlabel('Frequency [Hz]')
            plt.ylabel(r'$G^{\prime\prime}(f) [Pa]$')
        plt.tight_layout()
        plt.savefig(main_path+'/Gimag_from_MBR'+tau+'.png',format='png',bbox_inches='tight')
        plt.savefig(main_path+'/Gimag_from_MBR'+tau+'.pdf',format='pdf',bbox_inches='tight')
        

        
        j=j+1
        
#%%

'''Plot G deviation'''
sns.set(rc={'figure.figsize':(4,1.5)})
plt.rcParams.update({'font.size': 22})
df=pd.DataFrame.from_dict(G_real_deviation)

sns.set_theme(style='whitegrid')
plt.figure('G real error')
plt.title('Relative deviation')

#sns.boxplot(data=df, boxprops={'facecolor':'None'})
#sns.violinplot(data=df, zorder=.5,s=1,linewidth=0.1)
#sns.boxplot(data=df,linewidth=1, showfliers = False)
i=0
Mean_G_real_error=0
for name in G_real_deviation.keys():
    print(name)
    plt.errorbar(i,np.mean(G_real_deviation[name],0),np.std(G_real_deviation[name],0),capsize=5,marker='o',markeredgecolor='k',color=colors[i])
    Mean_G_real_error=Mean_G_real_error+abs(np.mean(G_real_deviation[name],0))
    i=i+1
Mean_G_real_error=Mean_G_real_error/len(G_real_deviation.keys())
print('Mean G real error: '+str(Mean_G_real_error)+'%')
plt.xticks(fontsize=10, rotation=45)
plt.yticks(np.arange(-10,10,0.5),fontsize=10)
plt.xticks([])
plt.yticks([0,.25,.5,.75,1])
plt.ylim([-0.1,1])
#plt.plot([-.5,6.5],[0,0],'r--',linewidth=2)
plt.ylabel('Relative deviation',fontsize=10)
plt.tight_layout()
filename='ErrorGreal'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 




'''Plot G imag deviation'''
sns.set(rc={'figure.figsize':(4,1.5)})
df=pd.DataFrame.from_dict(G_imag_deviation)

sns.set_theme(style='whitegrid')
plt.figure('G imag error')
plt.title('Relative deviation')

#sns.boxplot(data=df, boxprops={'facecolor':'None'})
#sns.violinplot(data=df, zorder=.5,s=1,linewidth=0.1)
#sns.boxplot(data=df,linewidth=1, showfliers = False)
i=0
Mean_G_imag_error=0
for name in G_imag_deviation.keys():
    print(name)
    plt.errorbar(i,np.mean(G_imag_deviation[name],0),np.std(G_imag_deviation[name],0),capsize=5,marker='o',markeredgecolor='k',color=colors[i])
    Mean_G_imag_error=Mean_G_imag_error+abs(np.mean(G_imag_deviation[name],0))
    i=i+1
Mean_G_imag_error=Mean_G_imag_error/len(G_real_deviation.keys())
print('Mean G imag error: '+str(Mean_G_imag_error)+'%')
plt.xticks(fontsize=10, rotation=45)
plt.yticks(np.arange(-10,10,0.5),fontsize=10)
plt.xticks([])
plt.yticks([0,.25,.5,.75,1])
plt.ylim([-0.1,1])
#plt.plot([-.5,6.5],[0,0],'r--',linewidth=2)
plt.ylabel('Relative deviation',fontsize=10)
plt.tight_layout()
filename='ErrorGimag'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 


'''Plot X imag deviation'''
Mean_Response_Error=0
sns.set(rc={'figure.figsize':(4,1.5)})
df=pd.DataFrame.from_dict(Response_deviation)
sns.set_theme(style='whitegrid')
plt.figure('X imag error')
plt.title('Relative deviation')
#sns.violinplot(data=df, zorder=.5,s=1,linewidth=0.1)
#sns.boxplot(data=df,linewidth=1, showfliers = False)
i=0
for name in Response_deviation.keys():
    print(name)
    plt.errorbar(i,np.mean(Response_deviation[name],0),np.std(Response_deviation[name],0),capsize=5,marker='o',markeredgecolor='k',color=colors[i])
    Mean_Response_Error=Mean_Response_Error+abs(np.mean(Response_deviation[name],0))
    i=i+1

Mean_Response_Error=Mean_Response_Error/len(G_real_deviation.keys())
print('Mean Response Error: '+str(Mean_Response_Error)+'%')
plt.xticks(fontsize=10, rotation=45)
plt.yticks(np.arange(-10,10,0.5),fontsize=10)
plt.xticks([])
plt.yticks([0,.25,.5,.75,1])
plt.ylim([-0.1,1])
#plt.plot([-.5,6.5],[0,0],'r--',linewidth=2)
plt.ylabel('Relative deviation',fontsize=10)
plt.tight_layout()

filename='ErrorResponse'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 


