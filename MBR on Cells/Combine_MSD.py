# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 10:28:17 2021

@author: Compare different tau parameters fpor the same celltype
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.font_manager as fm
#import pandas as pd
import matplotlib as mpl
import os
import pickle

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from matplotlib.lines import Line2D
import scipy.io as sio
#%%
plt.rcParams['figure.figsize'] = 5,4
#%%



'''Define functions'''
def getmatfiles(path):
    #Returns all the .mat files in the given directory
    #path='Y:/till/Till/Data/2019_05_08_active_passive_075agarose_after_realignment/scan'
    filenames=np.array(os.listdir(path))                            #lists all files in dir
    filenames= np.array([i for i in filenames if ".mat" in i])      #regard only .mat files
    return(filenames)

def loadData(path,number):
    import scipy.io as sio
    filenames=getmatfiles(path)
    emptyarray=np.array([])
    filepath=path+'/'+filenames[number]
    content=sio.loadmat(filepath)
    for k,v in content.items():
        if (isinstance(v,type(emptyarray)) and k!='None') :
            exec('%s=content[\'%s\']'%(k,k))                        #auto asign variable from matfile
    #print(filenames[number])
    del content
    return locals()

def fitMCD(time,mcd,p0):
    fitfun =lambda time,amp,timescale: amp-amp*np.exp(-1*timescale*time)
    
    popt, pcov = curve_fit(fitfun, time, mcd,p0=p0,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)
fitfun =lambda time,amp,timescale: amp-amp*np.exp(-1*timescale*time)

def fitEff(freq,eff,p0):
    fitfun =lambda freq,E0,E1: E0*freq**E1
    
    popt, pcov = curve_fit(fitfun, freq, eff,p0=p0,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)

#%%
    
root = tk.Tk()
root.withdraw()

main_path = filedialog.askdirectory()
os.chdir(main_path)







#%%

plt.rcParams['figure.figsize'] = 5/1.5,4/1.5
MCD_list=[]
lagtime_list=[]
full_msd_list=[]
taus_list=[]

os.chdir(main_path)
folders=[name for name in os.listdir(".") if os.path.isdir(name)]
for folder in folders:
    folderpath=main_path+r'/'+folder
    os.chdir(folderpath)
    experiments=[name for name in os.listdir(".") if os.path.isdir(name)]
    for experiment in experiments:
        experimentpath=folderpath+r'/'+experiment
        os.chdir(experimentpath)
        
        #Load MSD data
        with open(experimentpath+'/results_msd.pkl', 'rb') as f:
            [lagtimes,All_msd] = pickle.load(f)  
            
        msd=np.nanmean(np.asarray(All_msd),0)

        
        
        
        lagtime_list.append(lagtimes)
        
        full_msd_list.append(msd)

        
        plt.figure('MSD')
        #plt.plot(lagtimes,msd,'b.',alpha=0.01)

plt.plot(lagtimes,np.mean(full_msd_list,0),'b-',label='MSD')
plt.loglog()
plt.xlabel('Lagtime [s]')
plt.ylabel(r'MSD [$\mu m^2$]')


plt.plot(lagtimes,.005*lagtimes,'g--',label='slope 1')
#plt.plot(lagtimes,.002*lagtimes**0.7,'g-x',label='slope 0.7')
plt.title('Stemcell')
plt.legend()
plt.tight_layout()
plt.savefig(main_path+r'/final_msd.png')
plt.savefig(main_path+r'/final_msd.pdf')

plt.rcParams['figure.figsize'] = 5,4

#%% 

'''Load MCD data and see if its the same as derivative of msd'''
#calculate MCD from MSD




MSD=np.mean(full_msd_list,0)*10**-12

with open(main_path+'/results_msd.pkl', 'wb') as f:
    pickle.dump([lagtimes,np.asarray(full_msd_list)], f)

#crop msd at beginning 
MSD=MSD[4:]
MSDtime=lagtimes[4:]
plt.figure()
plt.loglog()
plt.plot(MSDtime,MSD)
plt.figure()
plt.plot(MSDtime,np.gradient(MSD,MSDtime))



#%%
T=310 # K
kb=1.38*10**(-23) #kg*m^2 / (s^2 *K)

index=2

gamma0=2*kb*T*MSDtime[index]/MSD[index]

prefactor=gamma0/(4*kb*T)

MCDbyMSD=0.5-prefactor*np.gradient(MSD,MSDtime)
MCDbyMSDfreq=MSDtime



with open(main_path+'/results_tau0p01.pkl', 'rb') as f:
#with open(main_path+'/results_tau0p01.pkl', 'rb') as f:
    MCD_results = pickle.load(f) 
    
    #Cut it off so it covers the same lagtimes as MCD
    cutoff=MCD_results[0][-1]
    cutoff_index=np.argmin(np.abs(MCDbyMSDfreq-cutoff))
    MCDbyMSD=MCDbyMSD[:cutoff_index]
    MCDbyMSDfreq=MCDbyMSDfreq[:cutoff_index]
    
    
    # fig, ax1 = plt.subplots()
    # ax1.set_title('MCD calculated and from MSD')
    # ax1.plot(MCD_results[0],MCD_results[1],color='blue',label='MCD calculated')
    # #ax1.set_xlim([0,0.5])
    
    # ax1.legend()
    # ax2 = ax1.twinx()
    # ax2.plot(MCDbyMSDfreq[4:],MCDbyMSD[4:],color='red',label='MCD from MSD')
    # #plt.fill_between(All_results[0], np.nanmean(All_results[1],0)-np.nanstd(All_results[1],0), np.nanmean(All_results[1],0)+np.nanstd(All_results[1],0),alpha=0.2)
    # ax1.set_xlabel('Lagtime [s]')
    # ax1.set_ylabel(r'$<x(t)>_{x_0,\tau,d}/x_0$',color='blue')
    # ax2.set_ylabel(r'$-\dfrac{d}{dt}MCD + 0.5$',color='red')
    # #ax2.set_xlim([0,0.5])
    # ax2.legend()
    # plt.tight_layout()

    plot_until=np.argmin(abs(MCD_results[0]-MCDbyMSDfreq[-1]))
    plt.figure('MCD calculated and from MSD')
    plt.title(r'MCD calculated and from MSD')
    plt.plot(MCD_results[0][:plot_until],MCD_results[1][:plot_until],label='MCD calculated',color='blue')
    plt.plot(MCDbyMSDfreq[2:],MCDbyMSD[2:],'r--',label='MCD from MSD')
    
    plt.xlabel('Lagtimes [s]')
    plt.ylabel('MCD')
    #plt.text(0.25, 0.3, r'$MCD=-\frac{\gamma_0}{4k_BT}\frac{d}{dt}MSD+\frac{1}{2}$')
    #plt.text(0.25, 0.3, r'$MCD=- \dfrac{\gamma_0}{4k_BT}$')
    plt.legend()

    plt.tight_layout()
    plt.show()
    
with open(main_path+'/all_results.pkl', 'rb') as f:
    data = pickle.load(f)
    
Eff_median=data['medians']['E median']
Eff_freq=data['medians']['freq']

plt.figure('Effective Energy Stemcell')
plt.title('Effective Energy Stemcell')
plt.plot(Eff_freq,Eff_median[0],color='blue')
plt.fill_between(Eff_freq,Eff_median[0]-Eff_median[1],Eff_median[0]+Eff_median[1],color='blue',alpha=0.25)
plt.semilogx()
plt.xlabel(r'Frequency [Hz]')
plt.ylabel(r'Effective Energy [$k_BT$]')
plt.tight_layout()
        


            
        