# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 17:55:08 2021

@author: tmuenker
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
from scipy import stats
import matplotlib.font_manager as fm
#import pandas as pd
import matplotlib as mpl
import os
import pickle

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from matplotlib.lines import Line2D


plt.rcParams['figure.figsize'] = 5,4
plt.style.use(['default'])
#%%
tau='0p01'
tau='0p0006'


root = tk.Tk()
root.withdraw()


MCD_list=[]
Eff_list=[]
lagtime_list=[]
full_mcd_list=[]
freq_list=[]
full_eff_list=[]

celltypepath = filedialog.askdirectory()
fig_path=celltypepath
os.chdir(celltypepath)
folders=[name for name in os.listdir(".") if os.path.isdir(name)]
for folder in folders:
    print(folder)
    folderpath=celltypepath+r'/'+folder
    os.chdir(folderpath)
    experiments=[name for name in os.listdir(".") if os.path.isdir(name)]
    for experiment in experiments:
        experimentpath=folderpath+r'/'+experiment
        os.chdir(experimentpath)
        
        #Load MCD data
        with open(experimentpath+'/results_tau'+tau+'.pkl', 'rb') as f:
            [lagtimes,All_mcd,file,D] = pickle.load(f)  
            
        mcd=np.nanmean(np.asarray(All_mcd),0)
        
        '''Use fit to determine MBR'''
        #temp_fit=fitMCD(lagtimes,mcd,[0.5,2000])
        #MCD_list.append(temp_fit[0])
        '''Use value at time to determine MBR'''
        MCD_list.append(mcd[np.argmin(abs(lagtimes-1))])            
        
        
        #Load effective energies
        with open(experimentpath+"/combined_new.pkl", 'rb') as f:
            data = pickle.load(f)  
        Eff=data['Eff']
        freq=data['freq']
        #temp_fit=fitEff(freq,Eff,[15,-1])
        Eff0Hz=Eff[np.where(freq==1)]
        #Eff_list.append(temp_fit[0])
        Eff_list.append(Eff0Hz[0])
        
        lagtime_list.append(lagtimes)
        full_mcd_list.append(mcd)
        
        freq_list.append(freq)
        full_eff_list.append(Eff)
        
#%%
time=lagtimes
mcd=np.nanmean(full_mcd_list,0)

mcd_at_1s=np.nanmean(MCD_list,0)


plt.figure('MBR MCF7')
plt.plot(time,mcd)

# Get E0 from MCD13.48659159
# a=-24.744354    #tau 0p01
# b=13.48659159  
a=-96.57    #tau 0p0006
b=46.93
E0_MCD=a*mcd_at_1s+b

plt.figure('Eff compard')
plt.semilogx()
plt.plot(freq,np.nanmedian(full_eff_list,0),'o',label='From AP')
plt.plot(freq,E0_MCD*freq**-1.+1,label='From MBR')

# Get response function
with open(celltypepath+'/all_results.pkl', 'rb') as f:
        data = pickle.load(f)
        
        temp_psd=data['medians']['psd median'][0]
        temp_freq=data['medians']['freq']
        temp_ResImag=data['medians']['Res imag median']
        temp_ResReal=data['medians']['Res real median']
        temp_Eff=data['medians']['E median'][0]
        temp_G_real=data['medians']['G real median']
        temp_G_imag=data['medians']['G imag median']

        
        
'''Interpolate PSD to longer frequencies with f^-1'''
def get_psd(freq,short_psd):
    A=short_psd[0]
    return (A*freq**(-2))

plt.figure('psd interpolated')
loooong_freq=np.linspace(1,2**(15),2**16)

plt.loglog()
plt.plot(temp_freq,temp_psd)
plt.plot(loooong_freq,get_psd(loooong_freq,temp_psd),'--')          

#temp_freq=loooong_freq
#temp_psd=get_psd(loooong_freq,temp_psd)
T=310 # K
kb=1.38*10**(-23) #kg*m^2 / (s^2 *K)

# C.append(temp_psd)
# temp_fit=fitEff(temp_freq, temp_Eff, [temp_Eff[0]])
# Eff_fit.append(temp_fit)
# E0.append(medEff[j])

#%%

#Compare effective energies
EffFromMCD=E0_MCD*freq**-1.+1
plt.figure('Eff MBR vs AP')
plt.semilogx()
plt.plot(freq,EffFromMCD,'--',label='From MBR')
plt.errorbar(freq,temp_Eff,data['medians']['E median'][1],marker='o',linestyle='',capsize=5,label='From AP')
plt.xlabel('Frequency [Hz]')#
plt.ylabel('Effective energy [kT]')
plt.legend()
plt.tight_layout()
plt.show()
filename='EfffromMBR'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 
#%%

plt.rcParams['figure.figsize'] = 4,2

#Get X" the dissipative part of response function
temp_resp_imag=np.pi*temp_freq*temp_psd/(E0_MCD*freq**-1.+1)/(kb*T)*10**-18
plt.figure('Response function from ap and MBR')
plt.title(r'New cell type MCF7: Response function $X^{\prime\prime}(f)$')
plt.loglog()
plt.plot(temp_freq,temp_resp_imag,'k--',label='From MBR')
#plt.plot(temp_freq,temp_ResImag[0],'ko',label='Truth')

plt.legend()
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'$X^{\prime\prime}(f)$')
plt.tight_layout()
filename='ResponsefromMBR'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 
#%%
'''Use fit to retrive G'''
def fitXimag(freq,Ximag,p):
    fitfun =lambda freq,A,alpha,B,beta: -1/(6*np.pi*0.5*10**-6)*(A*(2*np.pi*freq)**alpha*np.sin(np.pi*alpha/2)+B*(2*np.pi*freq)**beta*np.sin(np.pi*beta/2))/(A**2*(2*np.pi*freq)**(2*alpha)+B**2*(2*np.pi*freq)**(2*beta)+2*A*B*(2*np.pi*freq)**(alpha+beta)*np.cos(np.pi/2*(alpha-beta)))
    popt, pcov = curve_fit(fitfun, freq, Ximag,p0=p,maxfev=5000) #,bounds=([-20,1],[20,50])
    return(popt)
fitfun2 =lambda freq,A,alpha,B,beta: -1/(6*np.pi*0.5*10**-6)*(A*(2*np.pi*freq)**alpha*np.sin(np.pi*alpha/2)+B*(2*np.pi*freq)**beta*np.sin(np.pi*beta/2))/(A**2*(2*np.pi*freq)**(2*alpha)+B**2*(2*np.pi*freq)**(2*beta)+2*A*B*(2*np.pi*freq)**(alpha+beta)*np.cos(np.pi/2*(alpha-beta)))
def log_interp1d(xx, yy, kind='linear'):
    logx = np.log10(xx)
    logy = np.log10(yy)
    lin_interp = sp.interpolate.interp1d(logx, logy, kind=kind)
    log_interp = lambda zz: np.power(10.0, lin_interp(np.log10(zz)))
    return log_interp


long_freq=np.linspace(1,1024,10000)
#long_freq=np.linspace(temp_freq[0],temp_freq[-1],2**14)


xf = log_interp1d(temp_freq, temp_resp_imag)
resp_imag_interp=-xf(long_freq)

popt= fitXimag(long_freq,resp_imag_interp,[20,0.25,0.5,0.7])


#%% Also show fit
plt.rcParams['figure.figsize'] = 4,2
#Get X" the dissipative part of response function
plt.figure('Response function from ap and MBR')
plt.title(r'New cell type MCF7: Response function $X^{\prime\prime}(f)$')
plt.loglog()

plt.plot(long_freq,-fitfun2(long_freq,*popt),'r-',alpha=1,label='From MBR Fit')
plt.errorbar(temp_freq,temp_ResImag[0],temp_ResImag[1],linestyle='',marker='o',color='black',capsize=5,label='Truth')

plt.legend()
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'$X^{\prime\prime}(f)$')
plt.tight_layout()
filename='ResponsefromMBRwithFit'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 

res_mbr=-fitfun2(temp_freq,*popt)
res_ap=temp_ResImag[0]
Response_error=1-res_mbr/res_ap
Response_abs_error=abs(Response_error)
#Get R^2 value
SSres=np.sum((res_ap-res_mbr)**2)
SStot=np.sum((res_ap-np.mean(res_ap))**2)
resR2=1-SSres/SStot
print('Response error: '+str(np.mean(Response_error)))
print('Response abs error: '+str(np.mean(Response_abs_error)))
print('Response R2: '+str(resR2))




#%%


'''Now get G from this fit'''
A=popt[0]
alpha=popt[1]
B=popt[2]
beta=popt[3]
G_from_fit=A*(1j*2*np.pi*temp_freq)**(alpha)+B*(1j*2*np.pi*temp_freq)**(beta)
#%%
plt.rcParams['figure.figsize'] = 4,2

plt.figure('G from MBR')
plt.title(r'New cell type MCF7: Complex Modulus $G^*(f)$ ')
plt.loglog()
plt.plot(temp_freq,G_from_fit.real*10**(-6),color='blue',linestyle='-',label=r'$G^\prime(f)$ from MBR')
plt.errorbar(temp_freq,temp_G_real[0],yerr=temp_G_real[1],color='blue',marker='o',linestyle='',capsize=5,label=r'$G^{\prime}(f)$: Truth')
plt.plot(temp_freq,G_from_fit.imag*10**(-6),color='red',linestyle='-',label=r'$G^{\prime\prime}(f)$ from MBR')

greal_mbr=G_from_fit.real*10**(-6)
greal_ap=temp_G_real[0]
greal_error=1-greal_mbr/greal_ap
greal_abs_error=abs(greal_error)
SSres=np.sum((greal_ap-greal_mbr)**2)
SStot=np.sum((greal_ap-np.mean(greal_ap))**2)
resR2=1-SSres/SStot
print('G real error: '+str(np.mean(greal_error)))
print('G real abs error: '+str(np.mean(greal_abs_error)))
print('G real R2: '+str(resR2))



plt.errorbar(temp_freq,temp_G_imag[0],yerr=temp_G_imag[1],color='red',marker='o',linestyle='',capsize=5,label=r'$G^{\prime\prime}(f)$: Truth')
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'$G^*[Pa]$')
plt.legend()
plt.tight_layout()
filename='ComplexModulifromMBR'+tau
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 

gimag_mbr=G_from_fit.imag*10**(-6)
gimag_ap=temp_G_imag[0]
gimag_error=1-gimag_mbr/gimag_ap
gimag_abs_error=abs(gimag_error)
SSres=np.sum((gimag_ap-gimag_mbr)**2)
SStot=np.sum((gimag_ap-np.mean(gimag_ap))**2)
resR2=1-SSres/SStot
print('G imag error: '+str(np.mean(gimag_error)))
print('G imag abs error: '+str(np.mean(gimag_abs_error)))
print('G imag R2: '+str(resR2))
