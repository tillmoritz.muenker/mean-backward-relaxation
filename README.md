# Mean Backward Relaxation

Contains the skripts used for the data analysis of the MBR publication and a Julia module to perform simulations. 

## Information

Detailed information about the usage of the provided skripts is provided uppon reasonable request.
Experimental source data can be found at: 
https://owncloud.gwdg.de/index.php/s/GQ4KHRj4eHvwwnt

## Example

Downlaod the MBR_Example_Pack if you are interested in using the MBR

