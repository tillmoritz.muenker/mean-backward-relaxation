# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 16:41:47 2021

@author: tmuenker

Use on Horse and card folder to get
MCD(tau),MSD,PSD,PSD_gabriel,Effetive Energy

"""

import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import os
import pickle



results={}

#%%
'''Import trajectories'''
root = tk.Tk()
root.withdraw()

main_path = filedialog.askdirectory(title='Choose calibration file')
os.chdir(main_path)

with open(main_path+r'/traject.dat', 'rb') as f:
    data = pickle.load(f)    


time=data['Time']


'''Create array of Trjactories'''
#We will have 100 trajectories
#Each is a measurement of 1s with a rate of 65536 Hz
rate=65536
dt=1/rate
N=len(time)

traject = list(data.values())
traject = np.array(traject)
traject = traject [:-1]

#%%
'''Generate MCD'''
def getMCD2(t,p,tau,dmin,maxtime):

    
    p-=np.mean(p)
    dt=t[0]
     
    '''Now calculate MCD'''
    tau=int(-tau/dt) #tau in array increments
    
    lagtime=np.linspace(0,maxtime-dt,(500+1))
    lagtime=lagtime/dt
    lagtime=lagtime.astype(int)
    #Get indices of p_array that fulfill condition
    d_array=p[tau:-int(maxtime/dt)]-p[:-tau-int(maxtime/dt)]
    to_analyse=np.abs(d_array) > dmin
    #start analzsis after some time and until some time
    to_analyse[:1000]=False
    to_analyse[-1000:]=False
    mcd=[]
    for lag in lagtime:
        a=p[tau:-int(maxtime/dt)]
        b=p[tau+lag:-int(maxtime/dt)+lag]
        dist_array=a[to_analyse]-b[to_analyse]
        mcd.append(np.mean(dist_array/d_array[to_analyse]))
        #mcd.append(np.median(dist_array/d_array[to_analyse]))
        
    return([lagtime*dt,mcd,sum(to_analyse)])


maxtime=0.05 #0
tau=-0.0006 #-
dmin=0.001 # 

MCD=[]
i=0
for x in traject:
    
    [lagtimes,temp_mcd,NN]=getMCD2(time, x, tau, dmin, maxtime)
    MCD.append(temp_mcd)
    print(i)
    i=i+1
 
plt.figure('MCD')
plt.title('MCD')
plt.plot(lagtimes,np.nanmean(MCD,0))
plt.xlabel('Lagtime[s]')
plt.ylabel('MCD')

results['Mean MCD']=lagtimes,np.nanmean(MCD,0)
results['All MCD']=lagtimes,MCD
#%%
'''Get MSD'''
def getMSD(time,x,sampling_rate,maxtime=.5,increment=3):
    
    lag=np.linspace(0,int(maxtime*sampling_rate),int(maxtime*sampling_rate+1))
    lag=lag[::3]
    
    msd=[]
    for n in lag:
       n=int(n)

       if n==0:
           diff_sq=0
       else:
           diff = x[n:]-x[:-(n)] #this calculates r(t + dt) - r(t)
           diff_sq = diff**2
       MSD = np.mean(diff_sq)
       msd.append(MSD)
        
        
    
    return(lag/sampling_rate,msd)



MSD=[]
i=0
for x in traject:

    lagtimes,msd=getMSD(time,x,rate)
    MSD.append(msd)
    print(i)
    i=i+1

MSD=np.asarray(MSD)

plt.figure()
plt.title('MSD')
plt.loglog(lagtimes,np.mean(MSD,0),'.-')
results['Mean MSD']=lagtimes,np.nanmean(MSD,0)
results['All MSD']=lagtimes,MSD
#%%
'''Get D_trap'''
with open(main_path+r'/trap.dat', 'rb') as f:
    data2 = pickle.load(f)    


time=data2['Time']


'''Create array of Trjactories'''
#We will have 100 trajectories
#Each is a measurement of 1s with a rate of 65536 Hz
rate=65536
dt=1/rate
N=len(time)

forces = list(data2.values())
forces = np.array(forces)
forces = forces [:-1]
MSD_force=[]
i=0
for x in forces:

    lagtimes,msd_force=getMSD(time,x,rate)
    MSD_force.append(msd_force)
    print(i)
    i=i+1

MSD_force=np.asarray(MSD_force)

plt.figure('MSD force')
plt.title('MSD force')
plt.loglog(lagtimes,np.nanmean(MSD_force,0))
meanMSD_force=np.nanmean(MSD_force,0)
T=293.15
kb=1.38*10**(-23)
R=0.5*10**(-6)
idex=np.argmin(np.abs(np.asarray(meanMSD_force)-1.5e-5))

gamma0=2*kb*T*lagtimes[-1]/meanMSD_force[-1]*10**12
D_calculated_Trap=kb*T/gamma0

results['All MSD trap']=lagtimes,MSD_force
results['Mean MSD trap']=lagtimes,np.nanmean(MSD_force,0)
results['D trap']=D_calculated_Trap
results['gamma trap']=gamma0



#%%
'''Get PSD with Gabriels method'''
from numba import jit

#functions
#T Array of time steps for which the MSD is supposed to be evaluated
@jit(nopython=True)
def calcMSD(T,traject):       
    i = 0
    MSDs = np.zeros((len(T),2))   
    MSD_tmp = np.zeros((len(T),2)) 
    dt = traject[1,0] - traject[0,0]
    progress = 0.
    
    for t in T:       
        for k in  range(1,len(traject[0,0:])):             
            for j in range(0,len(traject[0:,k])-t):
                MSD_tmp[i,1] += np.power((traject[j+t,k] - traject[j,k]),2)  
                
            
            MSD_tmp[i,1] /= len(traject[0:,k])            
            MSDs[i,1] += MSD_tmp[i,1]
            MSD_tmp[i,1] = 0            
        
        
        if(np.around(i/len(T),2) != progress):
            progress = np.around(i/len(T),2)
            print(progress)
        
        MSDs[i,1] /= len(traject[0,1:])        
        MSDs[i,0] = t*dt
        i+=1    
    
    return MSDs
@jit(nopython=True)
def calc2ndDeriative(MSDs):
    derivative_ = np.zeros((len(MSDs[0:,0])-2,2))

    dt = MSDs[1,0]-MSDs[0,0]
    for i in range(1,len(MSDs[0:,0])-1):
        derivative_[i-1,0] =  MSDs[i,0]
        derivative_[i-1,1] =  (MSDs[i-1,1] + MSDs[i+1,1] - 2*MSDs[i,1]) / dt**2
    
    return derivative_    

def fourTransformMSD(MSD_2nd, D):
    δt = MSD_2nd[1,0] - MSD_2nd[0,0]

    freq_MSD = 2 * np.pi/ δt * np.fft.fftshift(np.fft.fftfreq(len(MSD_2nd[0:,0]))) 
    four_MSD = np.fft.fftshift(np.fft.fft(MSD_2nd[0:,1]))
    
    res = - np.asfarray([2*x.real for x in four_MSD]) / (freq_MSD**2) * δt - (4*D  / (freq_MSD **2)) 
    
    return freq_MSD, res

def MSD_four_exp(omega,D):    
    return -2 * 2 *D / omega**2 - 2* 2 * (1 - D) / (omega**2 + 1)

#%%
#Transform everything into the format that I load with Julia, saves some time in rewriting
transf = np.concatenate((time.reshape(len(time),1),traject.T),axis = 1)

print("Calculation starts...(ignore dividing by zero, too lazy :))")
t_max = int(0.5/dt)+1
###You can try 1 aswell if you want, was not that bad
step  = 10
###
T = np.arange(1,t_max,step)
print("Eveluating MSD...(numbers are percent)")
MSD = calcMSD(T,transf)
print("...MSD caculated!")
#%%
print("Computing FT...")
MSD_2nd = calc2ndDeriative(MSD)
#%%
print("Short time diffuion estimate:",(MSD[1,1] - MSD[0,1])/(MSD[1,0] - MSD[0,0]))
print("Long time MSD estimate:",np.max((MSD[0:,1])))
#The short time diffuion coefficient, estimated from the passive system (better you use the same for all systems)

#Short time diffusion from passive. Load calib file
calib=np.load(r'Z:\till\Till\PhD Data\Important experiments\MCD_Horse_Card\HorseCartTrajectories\0000\calibration.dat',allow_pickle=True)
D=calib['D fluid']*10**12
#D = 0.04797579739522123/2
#D=0.03026   #From Till


#The timescale of the spring, estimated from the long time MSD value of the passive system and the short time diffusion MSD(∞)/2D
scale = 0.00011708957232562482/(2*D)
#It would be better you would have a look onto that constants, D is important for everything, scale only for the theoretical result

freq_MSD,MSD_w = fourTransformMSD(MSD_2nd,D)
print("...FT computed")

#Calculate the expected values of the passive system
MSD_w_exp = D * scale**2 * MSD_four_exp(freq_MSD * scale,0.)

#Prepare for export
freq_MSD = freq_MSD.reshape(len(freq_MSD),1)
MSD_w = MSD_w.reshape(len(MSD_w),1)
MSD_w_exp = MSD_w_exp.reshape(len(MSD_w_exp),1)
res = np.concatenate((freq_MSD / (2*np.pi),MSD_w, MSD_w / MSD_w_exp),axis=1)
#%%
N=len(res[:,0])
start=int((N+1)/2)
freq_gabriel=res[start:,0]
psd_gabriel=abs(res[start:,1])
results['PSD gabriel']=freq_gabriel,psd_gabriel

#%%
'''Now calculate Effective energy with gabriels method'''
from scipy.interpolate import interp1d
#First load passive PSD
calib=np.load(r'Z:\till\Till\PhD Data\Important experiments\MCD_Horse_Card\HorseCartTrajectories\0000\calibration.dat',allow_pickle=True)

freq_passive,psd_passive=calib['PSD theoretical']





#Now interpolate active PSD along frequencies
PSD_interp = interp1d(freq_passive, psd_passive*2, kind='cubic')

PSD_passive_new=PSD_interp(freq_gabriel)

plt.figure('PSDs Gabriel')
plt.loglog(freq_gabriel,psd_gabriel,label='PSD active')
plt.loglog(freq_gabriel,PSD_passive_new,label='PSD passive')
plt.ylim([1e-11,1e-3])
plt.legend()
plt.show()

results['E eff gabriel']=freq_gabriel,psd_gabriel/PSD_passive_new

results['psd active gabriel']=freq_gabriel,psd_gabriel


plt.figure('Effective_energy gabriel')
plt.title('Effective_energy gabriel')
plt.ylim([-1,500])
plt.xlim([1,1000])
plt.semilogx(freq_gabriel,psd_gabriel/PSD_passive_new)
#%%
'''Calculate Effective Energy with standard method using PSD'''
def generatePSD(N,rate,data):
    #Generates the PSD for N samples measured with the frequency rate
    xdft=np.fft.fft(data)
    xdft=np.abs(xdft*np.conjugate(xdft))
    xdft=xdft/(N*rate)
    xdft.resize(int(N/2))
    return(xdft)

psd=[]
for x in traject:
    N=len(x)
    psd.append(generatePSD(N,rate,x))
    
    
meanPSD=np.mean(psd,0)
psd_freq=np.fft.fftfreq(N, d=1/rate)
psd_freq.resize(int(N/2))

PSD_interp = interp1d(psd_freq, meanPSD, kind='cubic')

PSD_active_new=PSD_interp(freq_passive)


plt.figure('PSDs standard')
plt.loglog(freq_passive,PSD_active_new,label='PSD active ')
plt.loglog(freq_passive,psd_passive,label='PSD passive')
plt.ylim([1e-11,1e-3])
plt.legend()
plt.show()

results['E eff standrad']=freq_passive,PSD_active_new/psd_passive

results['psd passive']=freq_passive,psd_passive
results['psd active standard']=freq_passive,PSD_active_new

plt.figure('Effective_energy standard')
plt.title('Effective_energy standard')
plt.ylim([-1,500])
plt.xlim([1,1000])
plt.semilogx(freq_passive,PSD_active_new/psd_passive)



#%%
with open(main_path+r'\Analysis.dat', 'wb') as f:
    pickle.dump(results, f)





