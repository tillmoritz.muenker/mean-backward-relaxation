module MBR
    using Random, Distributions
    export calcMBR!, diffusing_potential!
    
    """
        diffusing_potential!(dx,t,x,delta_t,para)
    Diffusing potential return Array(Float,2) (x,q) for parameters ```para = [D,D_q,k]```

    ...
    # Arguments
    - t::Float time
    - x::Array(Float,2) postion (Tracer x,Potential q)
    - delta_t::Float time step
    - para::Array(Float,1) Parameters for the diffusing potential para[1]=D_q/D
    ...
    # Example
    ```julia-repl
    julia> diffusing_potential(0,[0.,0.],0.01,[1.,0.5,1.])
    2-element Vector{Float64}:
     -0.0008984297052984061
      0.047427477125749004
    ```
    """    
    function diffusing_potential!(dx,t,x,delta_t,para)
    #para[1] = D_q/D  
    dx[1] = -(x[1] - x[2]) * delta_t + sqrt(2*delta_t)*rand(Normal())
    dx[2] = sqrt(2*delta_t * para[1])*rand(Normal())   
    end
    
     """
        tra_step(A,f,delta_t,para)

    Calculatates the next entry of trajectory with Euler-Maruyama for step function f(t,x,delta_t,para). x(t+delta_t) = x(t) + f(t,x,delta_t,para). The last entry on A is deleted.

    ...
    # Arguments
    - A::Array(Float,1+) The grid of the trajectory. Newest entry always at the front. First entry time, second postion. Length is conserved
    - f function with f:t,x,delta_t,para, return Array(Float,2); para::Array(Float) is specific for the function
    - delta_t::Float time_step
    ...

    # Example
    ```julia-repl
    julia>tra_step(A,diffusing_horse,0.1,[1.,0.,1.])
    ```
    """
    function tra_step!(dx,A,f,delta_t,para)
        #calc the next step
        t = A[1,1]
        f(dx,t,A[1,2:end],delta_t,para)
        #shift all entries one back
        work = circshift(A,(1,0))
        #add new entry on the top
        work[1,1] = t+delta_t
        for i in 1:length(dx)
            work[1,i+1] = A[1,i+1] + dx[i]
        end
        return work
    end
    
    """
        calcMBR!(timeMax,T,taus,delta_t,t_init,f,para,d=1,N=2)
        
    Calculates the MBR for step function f(t,x,delta_t,para) for the first coordinate x[1], the programs runs until timeMax seconds, return 1st column time, further columns MBR

    ...
    # Arguments
    - timeMax::double Running time after init in seconds        
    - T::Array(Int) The times the MBR is supposed to be evaluated in time steps
    - taus::Array(Int) The Taus the MBR is supposed to be calculated in time steps
    - delta t::double Size of the time step
    - t init::double Initialization time in time of the dynamics (NOT TIME STEPS)
    - f function with f:t,x,delta_t,para, return Array(Float,2); para::Array(Float) is specific for the function
    - d::double Threshold
    - N:int number of coordiantes
    ...
    
    # Example
    ```julia-repl
    julia>calcMBR!(5*60,Array(0:10:1000),[10],1e-2,1,diffusing_potential!,[0.1])
    julia>MBR.calcMBR!(5*60,Array(0:10:1000),[10],1e-2,1,MBR.diffusing_potential!,[0.1])
    ```
    """
    function calcMBR!(timeMax,T,taus,delta_t,t_init,f,para,d=0.1,N=2)
        #For binned average
        bins=3
        threshold = 1000
        #code
        t_max = maximum(T)
        tau_max = maximum(taus)

        time_slots = Int(t_max+tau_max)+1
        A = zeros(time_slots,N+1)
        dx = zeros(N)

        #Initalize
        t = - t_init - (t_max+tau_max)*delta_t
        A[1,1] = t
        while(A[1,1] < 0)
            A = MBR.tra_step!(dx,A,f,delta_t,para)
        end
        #Start calc        
        MBRs = zeros(length(T), length(taus)+1,bins)
        MBR_repeats = zeros(length(T), length(taus),bins)

        counter = 0
        start = time()
        while(time() - start < timeMax)
            for i in 1:length(taus)
                if(abs(A[end-taus[i],2] - A[end,2])>d)                    
                    for j in 1:length(T)                        
                        #update via running average \mu_N+1 = 1/(N+1) (x_N+1 + N * \mu_N)
                        MBRs[j,i+1,1] =   -(A[end-taus[i]-T[j],2] - A[end-taus[i],2])/(A[end-taus[i],2] - A[end,2])/(MBR_repeats[j,i,1] + 1) +  MBRs[j,i+1,1] * (MBR_repeats[j,i,1])/(MBR_repeats[j,i,1] +1)
                        MBR_repeats[j,i,1] += 1
                        #Do the binning average
                        for k in 1:bins-1
                            if MBR_repeats[j,i,k] >= threshold
                                MBRs[j,i+1,k+1] =  MBRs[j,i+1,k] / (MBR_repeats[j,i,k+1]+1) + MBRs[j,i+1,k+1] * MBR_repeats[j,i,k+1] / (MBR_repeats[j,i,k+1]+1)
                                MBR_repeats[j,i,k+1] += 1
                                MBR_repeats[j,i,k] = 0
                            end
                        end
                    end
                end
            end
            A = MBR.tra_step!(dx,A,f,delta_t,para)
            counter += 1
        end

       MBRs_result = zeros(length(T), length(taus)+1)

        for i in 1:length(T)
            MBRs_result[i,1] = T[i]*delta_t
            for j in 1:length(taus)
                #calculate the total number of points
                total = 0
                for k in 1:bins
                    total += MBR_repeats[i,j,k]  * threshold^k
                end
                weight = 0
                for k in 1:bins
                    weight = MBR_repeats[i,j,k] * threshold^k / total
                    MBRs_result[i,j+1] += MBRs[i,j+1,k] * weight
                end
            end
        end
        println("repeats:")
        println(MBR_repeats[1,1,1:end])
        return MBRs_result
    end
end
