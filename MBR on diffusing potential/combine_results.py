# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 15:07:42 2021

@author: Use Analysis.dat from everyfolder to combine results and plot figures
"""

import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import os
import pickle
from decimal import Decimal
from scipy.optimize import curve_fit
from scipy import interpolate
from scipy import stats
from matplotlib.lines import Line2D



colors=['royalblue','skyblue','springgreen','gold','tomato','red']

fig_path=r'Z:\till\Till\PhD Data\Important experiments\MCD_Horse_Card\HorseCartTrajectories'


plt.rcParams['figure.figsize'] = 5,4
#%%
'''Import Data'''
root = tk.Tk()
root.withdraw()

main_path = filedialog.askdirectory(title='Choose Main folder')
os.chdir(main_path)

#%%
folders=[d for d in os.listdir(os.getcwd()) if os.path.isdir(d)]

All_Results={}

for folder in folders:
    All_Results[folder]=np.load(main_path+r'/'+folder+r'/Analysis.dat',allow_pickle=True)
    print('Loading '+folder)
print('Loading done')
#%%
'''Load calibration data'''
All_Results['calibration']=np.load(main_path+r'/0000/calibration.dat',allow_pickle=True)

#%%
'''Define important constants'''
D_fluid=All_Results['calibration']['D fluid']
gamma=All_Results['calibration']['gamma']
kappa=All_Results['calibration']['kappa']
kappa_error=All_Results['calibration']['kappa error']
R=0.5*10**-6
eta=gamma/(6*np.pi*R)

fc=(kappa/(12*np.pi**2*eta*R))  #Corner frequency



#%%
'''Plot passive MCD with theory'''
passive_mcd=All_Results['0000']['All MCD']
mean_passive_mcd=np.mean(np.asarray(passive_mcd[1]),0)

plt.figure('Passive MBR')
plt.title('Passive MBR')

plt.fill_between(passive_mcd[0], np.min(passive_mcd[1],0), np.max(passive_mcd[1],0),color=colors[0],alpha=0.2)
plt.plot(passive_mcd[0],mean_passive_mcd,'b-',label=r'$MBR(t)_{experimental}$')

#Calculate the theoretical curve
def passive_theoretical(t,D_fluid,kappa):
    T=293.15
    kb=1.38*10**(-23)
    return(0.5*(1-np.exp(-D_fluid*t*kappa/(kb*T))))

plt.plot(passive_mcd[0],passive_theoretical(passive_mcd[0],D_fluid,kappa),'r-',label=r'$MBR(t)_{theoretical}$')
plt.xlabel('Lagtime[s]')
plt.ylabel('MBR')
plt.legend()
plt.tight_layout()
plt.show()
filename='Passive MBR'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')

#%%
'''Plot all MBR and fit them'''
def fitMCD_with_theory(time,mcd,p0):
    def fitfun2(time,Dt,Df,kappa):
        T=293.15
        kb=1.38*10**(-23)
        return((0.5-0.5*Dt/Df)*(1-np.exp(-1*Df*kappa*time/(kb*T))))
    
    popt, pcov = curve_fit(fitfun2, time, mcd,p0=p0,maxfev=1000000,bounds=([0,p0[1]-p0[1]*0.01,0.1*p0[2]],[10*p0[1],p0[1]+p0[1]*0.01,10*p0[2]]))
    return(popt,pcov)
def fitfun2(time,Dt,Df,kappa):
    
       T=293.15
       kb=1.38*10**(-23)
       return((0.5-0.5*Dt/Df)*(1-np.exp(-1*Df*kappa*time/(kb*T))))
    



All_MCD=[]
All_MCD_fits=[]
All_D_trap=[]
ratios=[] # D_trap, D_fluid,kappa
std_kappa=[]

All_MCD_error=[]

plt.figure('Mean MBRs')
plt.title('Mean MBRs')
i=0
for folder in folders:
    temp_time,temp_mcd=All_Results[folder]['Mean MCD']
    All_MCD_error.append(stats.sem(np.asarray(All_Results[folder]['All MCD'][1]),0)[-1])
    D_trap=All_Results[folder]['D trap']
    All_D_trap.append(D_trap)
    ratios.append(D_trap/D_fluid)
    All_MCD.append(temp_mcd)
    lagtime=temp_time
    
    plt.plot(lagtime,temp_mcd,color=colors[i],label='%.2f' % Decimal(str(D_trap/D_fluid)))
    #Now do the fitting
    temp_fit2,pcov=fitMCD_with_theory(lagtime,temp_mcd,[D_trap,D_fluid,kappa])
    std_kappa.append(np.sqrt(pcov[2][2]))
    All_MCD_fits.append(temp_fit2)
    plt.plot(lagtime,fitfun2(lagtime,*temp_fit2),color=colors[i],linestyle='--')
    i=i+1
    
    
#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
plt.text(0.005,0.1,r'Fit: $MBR(t)=(\frac{1}{2}-\frac{1}{2}\frac{D_t}{D})(1-e^{\frac{Dkt}{k_BT}})$')
plt.xlabel('Lagtime[s]')
plt.ylabel('MBR')
plt.tight_layout()
plt.show()
filename='Mean MBRs'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')


'''Plot prediction of MBR with known kappa D_trap and D_fluid'''
plt.figure('Predictd MBR')
plt.title('Predicted MBR')
i=0
for mcd in All_MCD:
    plt.plot(lagtime,mcd,color=colors[i],linestyle='-',label='%.2f' % Decimal(str(ratios[i])))
    plt.plot(lagtime,fitfun2(lagtime,All_D_trap[i],D_fluid,kappa),color=colors[i],linestyle='--')
    i=i+1
#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
plt.text(0.005,0.1,r'Prediction: $MBR(t)=(\frac{1}{2}-\frac{1}{2}\frac{D_t}{D})(1-e^{\frac{Dkt}{k_BT}})$')
plt.xlabel('Lagtime[s]')
plt.ylabel('MBR')
plt.tight_layout()
plt.show()   
filename='Predicted MBRs'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')

#%%

'''Plot 1. and 2. MBR for figure 1'''
plt.figure('Predictd MBR')
plt.title('MBR passive and active')
i=0
plt.plot([-1,1],[0.5,0.5],'k-',alpha=0.5)
for mcd in [All_MCD[0],All_MCD[1]]:
    plt.plot(lagtime,mcd,color=colors[i],linestyle='-',label='%.2f' % Decimal(str(ratios[i])))
    plt.plot(lagtime,fitfun2(lagtime,All_D_trap[i],D_fluid,kappa),color=colors[i],linestyle='--')
    i=i+1
    

#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
#plt.text(0.005,0.1,r'Prediction: $MCD(t)=(\frac{1}{2}-\frac{1}{2}\frac{D_t}{D})(1-e^{\frac{Dkt}{k_BT}})$')
plt.xlabel('Lagtime[s]')
plt.ylabel('MBR')
plt.xlim([-0.0025,0.0525])
plt.tight_layout()
plt.show()   
filename='First and second MBR'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')




#%%
'''Compare kappa'''
plt.figure('k estimated from MBR')
plt.title('k estimated from MBR')
i=0
for popt in All_MCD_fits:
    k_estimate=popt[2]
    plt.errorbar(ratios[i],k_estimate,yerr=std_kappa[i],color=colors[i],marker='o')
    i=i+1
plt.plot([ratios[0],ratios[-1]],[kappa,kappa],'r-',label='True k')
plt.fill_between([ratios[0],ratios[-1]],[kappa-kappa_error,kappa-kappa_error],[kappa+kappa_error,kappa+kappa_error],alpha=0.5,color='red')
plt.xlabel(r'$D_{trap}/D_{fluid}$')
plt.ylabel(r'Estimated k$[N/m]$')
plt.legend()
plt.tight_layout()
filename='k estimated form MBR'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')

    
#%%
'''Plot MBR vs ratio of D'''
custom_lines = [Line2D([0], [0], color='red', lw=4),
                Line2D([0], [0], color='red', lw=4),
                Line2D([0], [0], color='green', lw=4),
                ]


plt.figure('MBR vs ratio of D')
plt.title(r'MBR vs $D_{trap}/D_{fluid}$')
Longtime_MCD=[]
i=0
for mcd in All_MCD:
    Longtime_MCD.append(0.5-0.5*All_MCD_fits[i][0]/All_MCD_fits[i][1])
    plt.errorbar(0.5-0.5*All_MCD_fits[i][0]/All_MCD_fits[i][1],ratios[i],xerr=All_MCD_error[i],color=colors[i],marker='o',capsize=5)
    i=i+1
   
#Plot prediction
plt.plot(0.5-0.5*np.asarray(ratios),np.asarray(ratios),'r-')
plt.ylabel(r'$D_{trap}/D_{fluid}$')
plt.xlim([-1,0.55])
plt.ylim([-0.15,3])
plt.plot([0,0],[-0.15,3],'k--')
plt.plot([-1,0.55],[1,1],'k--')

plt.fill_between([0,0.55], -0.15,1,color='blue',alpha=0.1)
plt.fill_between([-1,0], 1,3,color='red',alpha=0.1)

plt.xlabel('Longtime MBR')
plt.legend(custom_lines,[r'Prediction: $\frac{1}{2}-\frac{1}{2} \frac{D_{Trap}}{D_{Fluid}}$'])
plt.legend(custom_lines,[r'Prediction: $\frac{D_{Trap}}{D_{Fluid}}=1-2MBR$'])
plt.tight_layout()
filename='MBR vs Ratio of D'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')
 

#%%
'''Plot all effective energies gabriel'''
All_Eff_gabriel=[]
All_predicted_Eff=[]
i=0
plt.figure('Effective energies gabriel')
plt.title(r'Effective energies $E_{Eff}(f)$ ')
for folder in folders:
    if i==0:
        temp_freq_passive,temp_eff=All_Results[folder]['E eff standrad'] # For first passive, we can't use gabriels method
    else:     
        temp_freq,temp_eff=All_Results[folder]['E eff gabriel']
        freq_gabriel=temp_freq
        
  
    All_Eff_gabriel.append(temp_eff)   
    i=i+1

#Convert frequency space from passive measurement into the same rnge as form active 
interpolatemcd = interpolate.interp1d(temp_freq_passive, All_Eff_gabriel[0])
All_Eff_gabriel[0]=interpolatemcd(freq_gabriel)
i=0
for folder in folders:
    E_predicted=(fc**2/freq_gabriel**2*All_D_trap[i]+D_fluid)/D_fluid
    All_predicted_Eff.append(E_predicted)
    #plt.loglog(freq_gabriel,E_predicted,color=colors[i],linestyle='--')
    plt.loglog(freq_gabriel,All_Eff_gabriel[i],color=colors[i],linestyle='-',marker='.',label='%.2f' % Decimal(str(ratios[i])))
    i=i+1

#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'$E_{eff}$ [$k_BT$]')

plt.plot([2,30],30000*np.asarray([2.,30.])**(-2),'k-')
plt.text(10,1000,'-2')

plt.xlim([1,1000])
plt.ylim([0.1,20000])
plt.tight_layout()
plt.show()  
filename='Eff gabriel'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')

#%%

'''Plot all effective energies'''
All_Eff_standrad=[]
i=0
plt.figure('Effective energies standard')
plt.title('Effective energies standard')
for folder in folders:
    temp_freq,temp_eff=All_Results[folder]['E eff standrad']
    freq=temp_freq
    All_Eff_standrad.append(temp_eff)
    plt.loglog(freq,temp_eff,color=colors[i],linestyle='-',marker='.',label='%.2f' % Decimal(str(ratios[i])))
    
    i=i+1
    
#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('Frequency [Hz]')
plt.ylabel(r'$E_{eff}$ [$k_BT$]')
plt.xlim([1,1000])
plt.ylim([0.1,20000])
plt.plot([2,30],50000*np.asarray([2.,30.])**(-2),'k-')
plt.text(10,1000,'-2')

plt.tight_layout()
plt.show() 
filename='Eff standard'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 


#%%
'''Plot effective energy at 16 Hz vs prediction'''



plt.figure('MBR vs Eff 16Hz')
plt.title('MBR vs Efff 16Hz')

i=0
eff_16_gabriel=[]
for eff in All_Eff_gabriel:
    eff16=eff[np.argmin(np.abs(freq_gabriel-16))]
    eff_16_gabriel.append(eff16)
    plt.errorbar(Longtime_MCD[i],eff16,xerr=All_MCD_error[i],color='red',marker='.')
    i=i+1
    
plt.plot(np.asarray(Longtime_MCD),(1-2*np.asarray(Longtime_MCD))*fc**2/16**2+1,'b-')
plt.xlabel('Longtime MBR')
plt.ylabel('Effective energy at 16Hz')
formular=r'$E_{eff}(16Hz)=(1-2MBR)\frac{f_c^2}{(16Hz)^2}+1$'
plt.text(-.5,50,formular,color='blue')
plt.legend(custom_lines,['Experiment','Theory'])
plt.tight_layout()
plt.show()
filename='MBR vs Eff 16Hz'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')


'''Plot effective energy at 2 Hz vs prediction'''
plt.figure('MBR vs Eff 2Hz')
plt.title('MBR vs Efff 2Hz')
i=0
eff_2_gabriel=[]
for eff in All_Eff_gabriel:
    eff2=eff[np.argmin(np.abs(freq_gabriel-2))]
    eff_2_gabriel.append(eff2)
    plt.errorbar(Longtime_MCD[i],eff2,xerr=All_MCD_error[i],color='red',marker='.')
    i=i+1
    
plt.plot(np.asarray(Longtime_MCD),(1-2*np.asarray(Longtime_MCD))*fc**2/2**2+1,'b-')
plt.xlabel('Longtime MBR')
plt.ylabel('Effective energy at 2Hz')
formular=r'$E_{eff}(2Hz)=(1-2MBR)\frac{f_c^2}{(2Hz)^2}+1$'
plt.text(-.5,3300,formular,color='blue')
plt.legend(custom_lines,['Experiment','Theory'])
plt.tight_layout()
plt.show()
filename='MBR vs Eff 2Hz'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')

#%%
'''Plot effective energy at different frequencies'''
custom_lines2 = [Line2D([0], [0], color='red', lw=4),
                Line2D([0], [0], color='blue', lw=4),
                Line2D([0], [0], color='orange', lw=4),
                ]


plt.figure('MBR vs Eff')
plt.title('MBRvs Efff')
i=0
eff_2_gabriel=[]
for eff in All_Eff_gabriel:
    eff2=eff[np.argmin(np.abs(freq_gabriel-2))]
    eff4=eff[np.argmin(np.abs(freq_gabriel-4))]

    eff16=eff[np.argmin(np.abs(freq_gabriel-16))]
    plt.errorbar(Longtime_MCD[i],eff2,xerr=All_MCD_error[i],color='red',marker='o',capsize=5)
    plt.errorbar(Longtime_MCD[i],eff4,xerr=All_MCD_error[i],color='blue',marker='o',capsize=5)

    plt.errorbar(Longtime_MCD[i],eff16,xerr=All_MCD_error[i],color='orange',marker='o',capsize=5)
    i=i+1
  
x_space=np.asarray([-2,0.5])    
  
plt.plot(np.asarray(x_space),(1-2*np.asarray(x_space))*fc**2/2**2+1,color='black',linestyle='-')
plt.plot(np.asarray(x_space),(1-2*np.asarray(x_space))*fc**2/2**4+1,color='black',linestyle='-')

plt.plot(np.asarray(x_space),(1-2*np.asarray(x_space))*fc**2/2**16+1,color='black',linestyle='-')
plt.xlabel('Longtime MBR')
plt.ylabel('Effective energy(f)')
formular=r'$E_{eff}(f)=(1-2MBR)\frac{f_c^2}{(f)^2}+1$'
plt.text(-.9,1900,'Expected:',color='black')
plt.text(-.9,1500,formular,color='black')
plt.legend(custom_lines2,['2Hz','4Hz','16Hz','Expected'],title=R'$E_{Eff}$ at')
plt.xlim([-1,0.55])
plt.ylim(-500,4200)

plt.tight_layout()
plt.show()
filename='MBR vs Eff different f'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')
    
    
    
#%%
'''Plot normalized effective Energy'''

#Eff = (fc^2/f^2*D_trap+D_fluied)/D_fluid
#-> rescale f=f/sqrt(fc^2*D_trap)
plt.figure('Eff rescaled')
#plt.title('Eff rescaled')    
i=0
for eff in All_Eff_gabriel:
    D_t=All_D_trap[i]
    print(D_t)
    freq_rescaled=freq_gabriel/(np.sqrt(fc**2*D_t/D_fluid))
    plt.loglog(freq_rescaled,eff,color=colors[i],linestyle='-',marker='.',label='%.2f' % Decimal(str(ratios[i])))
    i=i+1

#Plot expected master curve
long_freq=np.linspace(0.02,5000,10000)
plt.plot(long_freq,1/long_freq**2+1,'k--',label=r'$E_{eff}=$''\n''$f^{-2}+1$')

plt.xlabel(r'$f/\sqrt{f_c^2\cdot D_{trap}/D_{fluid}}$')
plt.ylabel(r'$E_{eff}$ [$k_BT$]')
plt.text(.1,1000,r'Rescaling: $f=\frac{f}{\sqrt{f_c^2\cdot \frac{D_{trap}}{D_{fluid}}}}$')
#plt.legend(title=r'$D_{trap}/D_{fluid}$',loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout()
plt.show() 
filename='Eff rescaled'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')     

#%% 
'''Fit Effective enenergies from Gabriel and plot E0 vs MBR'''
from scipy.optimize import curve_fit
def fitEff(freq,Eff):
    def fitfun(freq,E0):
        return E0*freq**-2+1
    popt, pcov = curve_fit(fitfun, freq, Eff)
    return(popt,pcov)
    
    
    


i=0
E_0=[]
E_0_error=[]
plt.figure('Eff fit')
plt.title('Eff fit')
for eff in All_Eff_gabriel:
    #Fit Eff
    popt,pcov=fitEff(freq_gabriel,eff)
    
    E0=(popt[0]-1)/fc**2
    
    
    E_0.append(E0)
    E_0_error.append(pcov[0])
    
    plt.loglog()
    plt.plot(freq_gabriel,eff,color=colors[i],linestyle='--',label='Eff')
    plt.plot(freq_gabriel,freq_gabriel**-2*E_0[-1]+1,color=colors[i],label='Fit')
    i=i+1
plt.tight_layout()
plt.show() 
filename='Eff_fitted'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 

plt.figure('E0vsMBR')
plt.title('E0 and MBR')
i=0
for E0 in E_0:
    plt.errorbar(Longtime_MCD[i],E0,xerr=All_MCD_error[i],color='black',marker='o',capsize=5,markeredgecolor='black',markerfacecolor=colors[i] )
    i=i+1
plt.plot(np.asarray(x_space),(1-2*np.asarray(x_space)),color='black',linestyle='-')
plt.xlim([-1,0.55])
plt.ylim([-0.1,3])
plt.xlabel('Longtime MBR')
plt.ylabel(r'$E_0$[$k_BT$]')
plt.tight_layout()
plt.show() 
filename='E0vsMBR'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight')    

#%%
'''Plot MBR only for passive and one active system'''

fitfun2


plt.rcParams['figure.figsize'] = 5,4
plt.figure('MBR passive and active')
plt.title('MBR passive and active')
i=0
for folder in [folders[0],folders[1]]:
    temp_time,temp_mcd=All_Results[folder]['Mean MCD']
    lagtime=temp_time
    plt.plot(lagtime[::15],fitfun2(lagtime,All_D_trap[i],D_fluid,kappa)[::15],'-',color=colors[i])
    plt.plot(lagtime[::5],temp_mcd[::5],'.',markeredgecolor='black',color=colors[i],label='%.2f' % Decimal(str(D_trap/D_fluid)))
    
    i=i+1
    
    
# Plot simulation    
simulation_passive = np.loadtxt(r'Z:\till\Till\PhD Data\Important experiments\MCD_Horse_Card\Simulations\MCD_D=0.0.dat')   
plt.plot(simulation_passive[::15,0]*gamma/kappa,simulation_passive[::15,1],'D',markeredgecolor='black',alpha=0.8,color=colors[0])

 
simulation_active = np.loadtxt(r'Z:\till\Till\PhD Data\Important experiments\MCD_Horse_Card\Simulations\MCD_D=0.18.dat')     
plt.plot(simulation_active[::15,0]*gamma/kappa,simulation_active[::15,1],'D',markeredgecolor='black',alpha=0.8,color=colors[1])
 
plt.xlim([-0.001,0.05])    
plt.xlabel('Time[s]')
plt.ylabel('MBR')
plt.tight_layout()
plt.show() 
filename='MBR passive and active'
plt.savefig(fig_path+'/'+filename+'.png',format='png',bbox_inches='tight')
plt.savefig(fig_path+'/'+filename+'.pdf',format='pdf',bbox_inches='tight') 


    