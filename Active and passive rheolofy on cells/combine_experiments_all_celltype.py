# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:00:39 2020

@author: Automatic analyse experiments in celltype folder
"""

import matplotlib.pyplot as plt
import numpy as np
import os
from textwrap import wrap
import tkinter as tk
from tkinter import filedialog
import pickle

import active_analysis as aa
import passive_analysis as pa
import combine_active_passive as cap 
#%%
#Plot style
plt.style.use('bmh')

#Fildedialog box
root = tk.Tk()
root.withdraw() 
#general_path=r'Z:\till\Till\Key_experiments\AP_correctedbeta'
general_path=r'Z:\till\Till\PhD Data\Important experiments\MCD_on_Cells\Drugged'
os.chdir(general_path)
Celltypes=[name for name in os.listdir(".") if os.path.isdir(name)]
for celltype in Celltypes:
    '''Open experiment path, should be the directory of a celltype'''
    '''Then go all the way down to the folders of single experiments'''
    main_path= general_path+'//'+celltype
    #Change dir
    os.chdir(main_path)
    #List all folders in dir
    dir_namesN=[name for name in os.listdir(".") if os.path.isdir(name)]
    #Get number of experiments in folder
    N_experimentsN=np.size(dir_namesN)
    '''Number the analysed experiments and init empty result array'''
    experiment_nr=0
    results={}
    '''Add up data to create medians'''
    E_eff=[]
    G_real=[]
    G_imag=[]
    Response_real=[]
    Response_imag=[]
    PSD=[]
    Force_spect=[]
    Dissi_spect=[]
    
    for sub in dir_namesN:
        sub_path=main_path+'\\'+sub
        os.chdir(sub_path)
        subdir_namesN=[name for name in os.listdir(".") if os.path.isdir(name)]
        N_experimentsN=np.size(dir_namesN)
        
        for subsub in subdir_namesN:
            print('Folder: '+sub+' Experiment: '+subsub)
            subsub_path=sub_path+'\\'+subsub
            '''Now load data from combined experiments'''
            combined_dict=pickle.load( open( subsub_path+"/combined_new.pkl", "rb" ) )
            
            '''Save all the data except the position signals'''
            combined_dict['meta']['Filename']=sub+' '+subsub
            exp_dict={
                    'meta':combined_dict['meta'],
                    'freq':combined_dict['freq'],
                    'G real':combined_dict['G real'],
                    'G imag':combined_dict['G imag'],
                    'Response real':combined_dict['Response real'],
                    'Response imag':combined_dict['Response imag'],
                    'psd':combined_dict['psd'],
                    'Eff':combined_dict['Eff'],
                    
                    }
            '''Calculate Force and dissipation spectrum'''
            
            '''Force'''
            kb=1.38064852*10**(-23) #J/K
            T=310   #K
            S_active=combined_dict['psd']/np.abs(combined_dict['Response real']**2+combined_dict['Response imag']**2) *10**(-24) #N**2
            S_passive=6*kb*T*0.5*10**(-6)*combined_dict['G imag']/combined_dict['freq'] #N**2
            
            '''Correct force spectrum for shif'''
            shift=1#S_passive[-1]/S_active[-1]
            S_active*=shift
            Force=S_active-S_passive
            
            # plt.figure('Force spect')
            # plt.loglog(combined_dict['freq'],S_active)
            # plt.loglog(combined_dict['freq'],S_passive)
            # plt.pause(0.01)
            # plt.cla()
            
            '''Dissipation'''
            #I =  2 kb T (T_Eff -T) / (1+(G'/G'')^2)
            Dissipation=np.abs(2*(combined_dict['Eff']-1))/(1+(combined_dict['G real']/combined_dict['G imag'])**2)
            
            
            exp_dict['Force spect']=Force
            exp_dict['Dissi spect']=Dissipation
            
            
            
            
            
            '''Now add this dict to the results dict'''#
            results[str(experiment_nr)]=exp_dict
            experiment_nr+=1
            
            '''Add up data to create median an sem'''
            f=combined_dict['freq']
            E_eff.append(combined_dict['Eff'])
            G_real.append(combined_dict['G real'])
            G_imag.append(combined_dict['G imag'])
            Response_real.append(combined_dict['Response real'])
            Response_imag.append(combined_dict['Response imag'])
            PSD.append(combined_dict['psd'])
            Force_spect.append(Force)
            Dissi_spect.append(Dissipation)
            
            '''Plot data live'''
            # plt.figure('Live plotting')
            # plt.title('E_eff')
            # plt.semilogx(f,combined_dict['Eff'])
            # plt.pause(0.01)
            # plt.show()
            
    
            
            
    print('Done loading data')
     
    #%%   
    '''Calculate medians and sem'''
    from scipy import stats
    E_median=[np.median(E_eff,0),stats.sem(E_eff,0)]
    G_real_median=[np.median(G_real,0),stats.sem(G_real,0)]
    G_imag_median=[np.median(G_imag,0),stats.sem(G_imag,0)]
    Res_real_median=[np.median(Response_real,0),stats.sem(Response_real,0)]
    Res_imag_median=[np.median(Response_imag,0),stats.sem(Response_imag,0)]
    PSD_median=[np.median(PSD,0),stats.sem(PSD,0)]
    Force_spect_median=[np.median(Force_spect,0),stats.sem(Force_spect,0)]
    Dissi_spect_median=[np.median(Dissi_spect,0),stats.sem(Dissi_spect,0)]
    
    Medians={
        'E median':E_median,
        'G real median':G_real_median,
        'G imag median':G_imag_median,
        'Res real median':Res_real_median,
        'Res imag median':Res_imag_median,
        'psd median':PSD_median,
        'Force spect median':Force_spect_median,
        'Dissi spect median':Dissi_spect_median,
        'freq':f
        }
    
    results['medians']=Medians
    
    '''Save dictionary for entire cell type'''
    file = open(main_path+"/all_results.pkl", "wb")
    pickle.dump(results, file)
    file.close()
    
    #%%
    
    '''Plot results'''
    plt.figure('E_eff')
    plt.title('Median E_eff')
    plt.semilogx()
    plt.errorbar(f,E_median[0],yerr=E_median[1])
    plt.show()
    
    plt.figure('Complex moduli')
    plt.title('Median moduli')
    plt.loglog()
    plt.errorbar(f,G_real_median[0],yerr=G_real_median[1],label='G real')
    plt.errorbar(f,G_imag_median[0],yerr=G_imag_median[1],label='G imag')
    plt.legend()
    plt.show()
    
    plt.figure('Force spect')
    plt.title('Median Force spectrum')
    plt.loglog()
    plt.errorbar(f,Force_spect_median[0],yerr=Force_spect_median[1])
    plt.show()
    
    plt.figure('Dissi spect')
    plt.title('Median dissipation spectrum')
    plt.loglog()
    plt.errorbar(f,Dissi_spect_median[0],yerr=Dissi_spect_median[1])
    plt.show()
    
    
    



