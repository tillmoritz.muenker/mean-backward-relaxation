# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 09:56:58 2019

@author: Till

This script analysizes the data of an active microrheology experiment
Selct the folder which contains the 'active' folder,created during active experiments. In this active folder you should 
find the different matlab files for each osciallation frequency.
At the end a txt file active.txt is save which contains the frequencies, storage modulus g', dissipative modulus G'' such as
real and imaginary response function.
When active and passive measurement is analysed, the data can be further processed with the combine_active_passive.py script
"""




def analyseactive(path):


    import matplotlib.pyplot as plt
    import numpy as np
    import os
    import pickle
    
    
    
    #%%
    '''Define functions to be used'''
    
    def getmatfiles(path):
        #Returns all the .mat files in the given directory
        #path='Y:/till/Till/Data/2019_05_08_active_passive_075agarose_after_realignment/scan'
        filenames=np.array(os.listdir(path))                            #lists all files in dir
        filenames= np.array([i for i in filenames if ".mat" in i])      #regard only .mat files
        return(filenames)
    
    def loadData(path,number):
        import scipy.io as sio
        filenames=getmatfiles(path)
        emptyarray=np.array([])
        filepath=path+'/'+filenames[number]
        content=sio.loadmat(filepath)
        for k,v in content.items():
            if (isinstance(v,type(emptyarray)) and k!='None') :
                exec('%s=content[\'%s\']'%(k,k))                        #auto asign variable from matfile
        #print(filenames[number])
        del content
        return locals()
    
    def generateFFT(N,rate,data):
        xdft=np.fft.fft(data)
        xdft.resize(int(N/2))
        norm=N*rate
        #norm=1
        xdft/=norm
        
        #Play around with dtft
        # factor=4
        # data_new=np.zeros(N*factor)
        # data_new[:N]=data-np.mean(data)
        
        
        # fft_new=np.fft.fft(data_new)
        # fft_new.resize(int(N*factor/2))
        # fft_new=fft_new/(N*rate)
        # freq_new=np.fft.fftfreq(N*factor,1/rate)[0:np.int(N*factor/2)]
        
        # freq_old=np.fft.fftfreq(N,1/rate)[0:np.int(N/2)]
        
        # indices = np.where(np.in1d(freq_new, freq_old))[0]
        # fft=fft_new[indices]
        # print(len(fft_new))
        # xdft=fft_new
        # print(len(xdft),len(fft_new))
        
        '''Compare spectra'''

        # plt.figure()
        # plt.loglog()
        # plt.plot(freq_old,np.abs(xdft),'-',label='abs dtft')
        # plt.plot(freq_old,np.abs(xdft.imag),'r--',label='imag')
        # plt.plot(freq_old,np.abs(xdft.real),'g--',label='real')
        # plt.legend()
        # plt.show()
        
        return(xdft)
    
    def getPeak(data,freq,f_get):
        #picks out the datapoint of data at the frequency f_get. The frequency array freq has to be included
        index=np.where(freq==f_get)[0]  #usually active
        #index=np.argmin(np.abs(freq-f_get)) #only active when analsing data from sebastian
        index=np.int(index)
        search_range=np.int(np.around(f_get/512*5))+1
        #search_range=0  #Also only active for sebastian
       #print('Sarch Range: ',search_range*freq[1],' Freq= ',f_get)
        peak_position=np.argmax(np.abs(data[index-search_range:index+search_range]))+index-search_range
        print(f_get,freq[peak_position],freq[index])
        if search_range==0:
            peak_real=data[index].real
            peak_imag=data[index].imag
    
        else:
            peak_position=np.argmax(np.abs(data[index-search_range:index+search_range]))
            # peak_real=data[np.where( np.abs(data.real)==np.max(np.abs(data[index-search_range:index+search_range].real)))].real
            # peak_imag=data[np.where( np.abs(data.imag)==np.max(np.abs(data[index-search_range:index+search_range].imag)))].imag
            peak_real=data[peak_position+index-search_range].real
            peak_imag=data[peak_position+index-search_range].imag
    
    
    
        peak=np.complex(peak_real,peak_imag)
    #    plt.figure()
    #    plt.loglog(freq[1:],np.abs(data[1:].real),label='real')
    #    plt.loglog(f_get,np.abs(peak_real),'o')
    #    plt.loglog(f_get,np.abs(peak_imag),'o')
    #    plt.loglog(freq[1:],np.abs(data[1:].imag),label='imag')
    #    plt.legend()
    #    plt.show()
        #print(peak)
        return(peak)
    
    #%%


    
    #Choose active folder
    path+='/active'
    
    #Add a folder to save the figures in
    figfolder=path+'/figures/'
    try:
        os.mkdir(path+'/figures')
    except OSError: 
        i=0
        #print ("Creation of the directory %s failed" % path)
    else:  
        i=0
        #print ("Successfully created the directory %s" % path)
    
    
    '''Get the number of matfiles in folder, each file corresponds to a certain frequency'''   
    numberoffiles=getmatfiles(path).size
    #Save all files in a list
    files=getmatfiles(path)
    plt.ioff()
    
    '''Bead radius in mu m'''
    radius=0.5  #in mu m
    #radius=1
 
    
    
    '''Decide if I use x or y data'''
    vars=loadData(path,0)
    constants=vars['constants'][0]
    xory=np.str(vars['active'][0])
    
    if xory == 'X':
        alpha=constants[0]
        beta=np.abs(constants[2])
        pos_string='pos_x'
        force_string='forcex'
        mirror_string='mirror_freq_x'
    else:
        alpha=constants[1]
        beta=np.abs(constants[3])
        pos_string='pos_y'
        force_string='forcey'
        mirror_string='mirror_freq_y'
        
    print('Alpha='+str(alpha))
        
    '''Adjust beta by hand'''
    #beta=0.17
        
    
    
    '''Init empty arrays for force peaks and position peaks'''
    peaks=np.zeros((numberoffiles,2),dtype=complex)
    forces=np.zeros((numberoffiles,2),dtype=complex) 
    
    
    '''Fluctuations'''
    '''iterate over all files and save peak fluctuation at every frequency'''
    for i in range(numberoffiles):
        '''Open up all required varibales in a that matfile'''
        vars=loadData(path,i)
        rate=int(vars['rate'])
    
    
        N=int(vars['N'])
        x=vars[pos_string][0]
    
        sumxy=vars['pos_xy'][0]
        x/=sumxy
        N=len(x)
         
        
        '''convert x from V in um'''
        x=x/beta     #um
        '''Generate the Fourier transfrom'''
        psdx=generateFFT(N,rate,x) #um/Hz
        
        '''Generate Frequency space'''
        freq=np.fft.fftfreq(N,1/rate)[0:np.int(N/2)]
    
        '''Get driving frequency in this mat file'''
        f_mirror=vars[mirror_string]
        
        #print('Force, rate:',rate,' N=',N,' Freq=',f_mirror)
    
        # plt.figure('Amplitude %f'%(f_mirror))
        # plt.plot(x)
        # plt.show()
        
    
    
        time=vars['t'][0]
        '''Returns the peak of the FFT at the driving frequency f_mirror'''
        peak=getPeak(psdx,np.around(freq,decimals=2),np.float(f_mirror[0]))
        maximum_disp=np.argmax(psdx[1:])
        peakpos = np.where( np.abs(psdx[1:]) == np.max(np.abs(psdx[1:])) )[0][0] +1
        
        peaks[i,1]=peak
        peaks[i,0]=f_mirror
     
        # '''Do some plotting'''
        # plt.figure('Real posFrequeny %f Hz'%f_mirror)
        # plt.xlabel('Frequency Hz')
        # plt.ylabel('PSD[um^2/Hz]')
        # plt.title('Real Position %i Hz'%f_mirror)
        # plt.loglog(peaks[i,0].real,np.abs(peaks[i,1].real),'o')
        # plt.loglog(freq[1:],np.abs(psdx[1:].real))
        # plt.savefig(figfolder+'Real posFrequeny %i Hz.png'%f_mirror)
        # plt.show()
        # # #
        # # #
        # plt.figure('Imag pos Frequeny %f Hz'%f_mirror)
        # plt.xlabel('Frequency Hz')
        # plt.ylabel('PSD[um^2/Hz]')   
        # plt.title('Imag Position %i Hz'%f_mirror)
        # plt.loglog(peaks[i,0].real,np.abs(peaks[i,1].imag),'o')
        # plt.loglog(freq[1:],np.abs(psdx[1:].imag))   
        # plt.show()
        # plt.savefig(figfolder+'Imag pos Frequeny %i Hz.png'%f_mirror)
        # plt.figure()
        # plt.plot(vars['t'][0],x,label='Position at %f Hz'%f_mirror)
        # plt.xlabel('Time a.u.')
        # plt.ylabel(r'Displacement [$\mu m$]')
        # plt.legend()
#     
#     
        plt.figure('Peaks',figsize=(5,5/1.618))
        plt.xlabel(r'Frequency [Hz]')
        plt.ylabel(r'FT position [$\mu$m/Hz]')   
        plt.title('FT Position')
        plt.loglog(peaks[i,0].real,np.abs(peaks[i,1]),'bo',label='Extracted Peaks')
        plt.loglog(freq[1:],np.abs(psdx[1:])) 
        plt.tight_layout()
        plt.show()
        plt.savefig(figfolder+'FFT_pos.png',format='png',bbox_inches='tight')
        plt.savefig(figfolder+'FFT_pos.pdf',format='pdf',bbox_inches='tight')
         
        
    #%%
        
    
    '''Forces'''
    '''iterate over all files and save peak force at every frequency'''
    
    for i in range(numberoffiles):
        vars=loadData(path,i)
        rate=int(vars['rate'])
        f_mirror=vars[mirror_string]
        force=vars[force_string][0]*alpha #pN
        N=len(force)
        # plt.figure('Applied Forces')
        # plt.xlabel('Time (a.u.)')
        # plt.ylabel('Force [pN]')
        # plt.plot(force,label='Force at %f Hz'%f_mirror)
        # plt.legend()
        # plt.show()
    #    
    #    plt.figure('Applied Force')
    #    plt.semilogx(f_mirror,np.abs(np.max(force)-np.min(force)),'o-')
    #    plt.ylim([0,100])
    #    plt.xlabel('Frequency [Hz]')
    #    plt.ylabel('Applied Force [pN]')
    
    
    
    
        freq=np.fft.fftfreq(N,1/rate)[0:np.int(N/2)]
        fft_force=generateFFT(N,rate,force)  #pn/Hz
    
        
        peakpos = np.where( np.abs(fft_force[1:]) == np.max(np.abs(fft_force[1:])) )[0][0] +1
        peak=getPeak(fft_force,np.around(freq,decimals=2),f_mirror[0])
        maximum_force=np.argmax(fft_force[1:])
        forces[i,1]=peak
        forces[i,0]=f_mirror
    
        '''Do sime plotting'''     
#        plt.figure('Force %f'%(f_mirror))
#        plt.plot(force)
#        plt.show()
#
#    
#        plt.figure('Real Force %f Hz'%f_mirror) 
#        plt.title('Real Force %f Hz'%f_mirror)
#        plt.loglog(f_mirror,np.abs(fft_force[peakpos].real),'o')
#        plt.loglog(freq[1:],np.abs(fft_force[1:].real))
#
#        plt.savefig(figfolder+'Real Force %i Hz.png'%f_mirror)
#
#       
#        plt.figure('Imag Force %f Hz'%f_mirror)
#        plt.title('Imag Force %f Hz'%f_mirror)
#        plt.loglog(f_mirror,np.abs(fft_force[peakpos].imag),'x')
#        plt.loglog(freq[1:],np.abs(fft_force[1:].imag))
#
#        plt.savefig(figfolder+'Imag Force %i Hz.png'%f_mirror)
       
        
#        fb_mirror_y=vars['fb_mirror_y'][0]
#        plt.figure()
#        plt.plot(fb_mirror_y)
#       
        plt.figure('Peaks force',figsize=(5,5/1.618))
        plt.xlabel(r'Frequency [Hz]')
        plt.ylabel(r'FT force [pN/Hz]')   
        plt.title('FT Force')
        plt.loglog(forces[i,0].real,np.abs(forces[i,1]),'bo')
        plt.loglog(freq[1:],np.abs(fft_force[1:]))   
        plt.tight_layout()
        plt.show()
        plt.savefig(figfolder+'FFT_force.png',format='png',bbox_inches='tight')
        plt.savefig(figfolder+'FFT_force.pdf',format='pdf',bbox_inches='tight')
        
        
        
      
    
        
    '''Here I sort forces and peaks array so that their frequency is in order'''
    
    order=np.argsort(forces[:,0],axis=0) #Looking at frequency axis
    forces=forces[order]
    order=np.argsort(peaks[:,0],axis=0)
    peaks=peaks[order]  

 
    
    '''Calculate and plot results'''
    
    response=peaks[:,1]/forces[:,1]#um/pN
    



    G=1./(6*np.pi*radius*response)   #Pa
    
#    plt.figure('x(w)',figsize=(5,5/1.618))#um
#    plt.loglog(peaks[:,0].real,np.abs(peaks[:,1].real),'o-',label='Real')
#    plt.loglog(peaks[:,0].real,np.abs(peaks[:,1].imag),'o-',label='Imag')
#    plt.loglog(peaks[:,0].real,np.abs(peaks[:,1]),'o-',label='Absolute')
#    plt.tight_layout()
#    plt.legend()
#    plt.xlabel('Frequency Hz')
#    plt.ylabel('Peaks')
#    plt.savefig(figfolder+'fluctuations.pdf',format='pdf',bbox_inches='tight')
#    plt.savefig(figfolder+'fluctuations.png',format='png',bbox_inches='tight')
#    
#    plt.figure('F(w)',figsize=(5,5/1.618))#pN
#    plt.loglog(np.abs(forces[:,0]),np.abs(forces[:,1].real),'o-',label='Real')
#    plt.loglog(np.abs(forces[:,0]),np.abs(forces[:,1].imag),'o-',label='Imag')
#    plt.loglog(np.abs(forces[:,0]),np.abs(forces[:,1]),'o-',label='Absoulte')
#    plt.xlabel('Frequency[Hz]')
#    plt.ylabel(r'Force[$pN^2/s$]')
#    plt.tight_layout()
#    plt.legend()
#    plt.savefig(figfolder+'Force.pdf',format='pdf',bbox_inches='tight')
#    plt.savefig(figfolder+'Force.png',format='png',bbox_inches='tight')
#    
#    
#    
#    plt.figure('Respose X(w)',figsize=(5,5/1.618))
#    
#    plt.loglog(forces[:,0].real,np.abs(response.real),'.-',label='Real')
#    plt.loglog(forces[:,0].real,np.abs(response.imag),'.-',label='Imag')
#    plt.loglog(forces[:,0].real,np.abs(response),'.-',label='Absolute')
#    plt.title('Response function')
#    plt.xlabel('Frequency[Hz]')
#    plt.ylabel(r'Response $\tilde{\chi}$(w)')
#    plt.tight_layout()
#    plt.legend()
#    plt.savefig(figfolder+'response.pdf',format='pdf',bbox_inches='tight')
#    plt.savefig(figfolder+'response.png',format='png',bbox_inches='tight')
#    plt.show()
        
    plt.figure('G',figsize=(5,5/1.618))

    plt.loglog(forces[:,0].real,G.real,'b-',label='G\'')
    plt.loglog(forces[:,0].real,G.imag,'r-',label='G\"')
    plt.title('Shear modulus')
    plt.xlabel('Frequency[Hz]')
    plt.ylabel('Shear Modulus [Pa]')
    plt.tight_layout()
    plt.legend()
    plt.savefig(figfolder+'gs.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(figfolder+'gs.png',format='png',bbox_inches='tight')
    plt.show()
    
    
    #%% 
    '''Now save important data in file. The format tis dictionary'''
    meta={
          'rate':rate,
          'xory':xory,
          'alpha':alpha,
          'beta':beta,
          
          }
    
    
    to_save={
            'meta':meta,
            'freq':np.abs(forces[:,0]),
            'G real':G.real,
            'G imag':G.imag,
            'Response real':response.real,
            'Response imag':response.imag
            }
    '''Save dict to pickle'''
    
    
    
    
    #%%
    
    file = open(path+"/active_new.pkl", "wb")
    pickle.dump(to_save, file)
    file.close()
    print('Active done')
    
    #%%plot force and position
   
    vars=loadData(path,5)
    rate=int(vars['rate'])
    time=vars['t'][0]
    
    N=int(vars['N'])
    x=vars[pos_string][0]
    
    sumxy=vars['pos_xy'][0]
    x/=sumxy
    N=len(x)
     
    
    #convert x from V in um
    x=x/beta     #um
    
    rate=int(vars['rate'])
    f_mirror=vars[mirror_string]
    force=vars[force_string][0]*alpha #pN
    N=len(force)
    
    
#    plt.figure('Position',figsize=(5,5/1.618))
#    plt.plot(time,x)
#    plt.xlabel('Time')
#    plt.ylabel('Position')
#    plt.tight_layout()
#    
#    
#    
#    fig, ax1 = plt.subplots(figsize=(5,5/1.618))
#    
#    x=x-np.mean(x)
#    force=force-np.mean(force)
#    
#    color = 'tab:blue'
#    ax1.set_xlabel('Time [s]')
#    ax1.set_ylim([np.min(x),np.max(x)])
#    ax1.set_ylabel(r'Position [$\mu m$]',color=color)
#    ax1.plot(time,x,color=color)
#    ax1.tick_params(axis='y', labelcolor=color)
#    
#    
#    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
#    
#    color = 'tab:red'
#    ax2.set_ylabel(r'Force [pN]', color=color)  # we already handled the x-label with ax1
#    ax2.set_ylim([np.min(force),np.max(force)])
#    ax2.plot(time,force, color=color)
#    ax2.tick_params(axis='y', labelcolor=color)
#    
#    fig.tight_layout()
#    plt.show()

    #Save that data for plotting paper figures:
   # data_to_save=[time,x,force]
    #np.savetxt(r'Y:\till\Till\Paper\v02\F1\Data\active.txt',data_to_save,delimiter=',')
  
    
if __name__ == '__main__':
    '''Chose the path of your experiment'''
#    general_path=r'C:\Users\timo\Documents\Temp Data'
 #   general_path=r'C:\Users\timo\Desktop\211201 OT mouse oocytes\Measurement13'
    general_path=r'C:\Users\timo\Documents\Temp Data\b06'
        
    analyseactive(general_path)