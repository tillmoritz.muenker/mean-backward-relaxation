# -*- coding: utf-8 -*-
"""
Created on Wed May 15 09:57:46 2019

@author: Till
"""
#Here I combine results from active and passive rheology experiment


def combineactivepassive(general_path):

    import matplotlib.pyplot as plt
    import numpy as np
    import os
    import pickle
    import copy
    
    
    #%%%
    #Plot style
    plt.style.use('bmh')
    
    
    
    '''Do you want to normalize too high freq'''
    norm=False
    factor=1
    
    '''Path and filenames for the active experiment'''
    path_active=general_path+'/active'
    
    
    '''Path and filenames for the passive experiment'''
    path_passive=general_path+'/passive'
    
    '''Load passive and active results'''
    passive_dict=pickle.load( open( path_passive+"/passive_new.pkl", "rb" ) )
    active_dict=pickle.load( open( path_active+"/active_new.pkl", "rb" ) )
    
    freq_bin=active_dict['freq']
    Cx=passive_dict['psdx']
    Cy=passive_dict['psdy']
    
    '''Depending on if we oscillate in x or y chose psd'''
    xory=active_dict['meta']['xory']
    if xory == 'Y':
        C=Cy #um^2
    else:
        C=Cx
    
    '''Use data from active'''
    freq=active_dict['freq']
    index=np.where(freq==1)
    index=np.int(index[0])
    freq=freq[index:]
    G_real=np.abs(active_dict['G real'][index:])
    G_imag=np.abs(active_dict['G imag'][index:])
    response_real=np.abs(active_dict['Response real'][index:])
    response_imag=np.abs(active_dict['Response imag'][index:])
    
    '''Constants and temperature'''
    kb=1.38064852*10**(-23) #J/K
    T=310   #K
    
    '''calculate passive,active response and effective energy'''
    response_passive=copy.deepcopy(C)*np.pi*freq_bin/(kb*T) *(10**-18) #um/pN
    response_active=copy.deepcopy(response_imag) #um/pN
    f_width=np.size(response_active)
    
    '''give option to normalize too high freq'''
    if norm:
            
        factor=np.mean(response_passive[9:]/response_active[9:])
        response_active*=factor

        '''Also correct everything origin from active with this factor'''
        response_imag*=factor
        response_real*=factor
        G_real/=factor
        G_imag/=factor
        
            
    e_eff=response_passive[:f_width]/response_active
    #%%
     
    
    '''Now you can plot the results'''
    #Plot passive:
    
    plt.figure(r'Passive Fluctuations',figsize=(5,5/1.618))
    plt.loglog(freq_bin,C,'.',label=r'Fluctuations C(f) ')
    plt.ylabel(r'Fluctuations C(f) [$\mu m^2/Hz$]')
    plt.xlabel(r'Frequnecy [$Hz$]')
    plt.tight_layout()
    plt.legend()
    plt.show()
    
    plt.figure(r'Apparent Response',figsize=(5,5/1.618))
    plt.loglog(freq_bin[:f_width],response_passive[:f_width],'x--',label=r'$\dfrac{C(f)\pi f}{ k_B T}$') #um^2/Hz
    plt.loglog(freq,response_active,'o--',label=r'$\chi (f)$ ') #um^2/Hz
    plt.xlabel(r'Frequency [Hz]')
    plt.ylabel(r'Response [m/N]')
    plt.ylim(10**-6,1)
    plt.legend()
    plt.tight_layout()
    plt.savefig(path_active+'/Apparent_Response.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(path_active+'/Apparent_Response.png',format='png',bbox_inches='tight')
    plt.show()
    
    plt.figure(r'Apparent Response 2',figsize=(5,5/1.618))
    plt.loglog(freq,C*np.pi*freq_bin/(kb*T) *(10**-18),'x--',label=r'$\dfrac{C(f)\pi f}{ k_B T}$') #um^2/Hz
    plt.loglog(freq,response_imag,'o--',label=r'$\chi (f)$ ') #um^2/Hz
    plt.xlabel(r'Frequency [Hz]')
    plt.ylabel(r'Response [m/N]')
    plt.ylim(10**-6,1)
    plt.legend()
    plt.tight_layout()
    plt.savefig(path_active+'/Apparent_Response.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(path_active+'/Apparent_Response.png',format='png',bbox_inches='tight')
    plt.show()

    
    plt.figure(r'Effective Energy',figsize=(5,5/1.618))
    plt.semilogx(freq_bin[:f_width],e_eff,'o--',label='Effective Energy')
    #plt.loglog(freq_bin[:f_width],e_eff,'o-',label='Effective Energy')
    plt.xlim(0,1500)
    plt.xlabel(r'Frequency[Hz]')
    plt.ylabel(r'Effective Energy [$k_B$T]')
    plt.ylim(-1,30)
    plt.hlines(1,0,1100)
    plt.yticks([1,5,10,15,20,25,30])
    plt.legend()
    plt.tight_layout()
    plt.savefig(path_active+'/EffectiveEnergy.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(path_active+'/EffectiveEnergy.png',format='png',bbox_inches='tight')
    plt.show()
    
    plt.figure(r'Complex modulus',figsize=(5,5/1.618))

    plt.loglog(freq_bin[:f_width],G_real,'o--',label='G\'')
    plt.loglog(freq_bin[:f_width],G_imag,'o-',label='G\"')

    plt.xlabel(r'Frequency[Hz]')
    plt.ylabel(r'Shear modulus [Pa]')
    plt.legend()
    plt.tight_layout()
    plt.savefig(path_active+'/G_factor.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(path_active+'/G_factor.png',format='png',bbox_inches='tight')
    plt.show()

    plt.figure(r'Ratio of moduli')
    plt.loglog(freq_bin[:f_width],G_real/G_imag,'o--',label='G\'')
         
    
    
    #%%
    '''Now save all the import results to a dictionary'''
    
    meta={
          'factor':factor,
          'xory':xory,
          'betay':passive_dict['meta']['betay'],
          'betax':passive_dict['meta']['betax'],
          'alpha':active_dict['meta']['alpha'],
          'rate':active_dict['meta']['rate']
          }
    to_save={
            'meta':meta,
            'freq':freq_bin[:f_width],
            'G real':G_real,
            'G imag':G_imag,
            'Response real':response_real,
            'Response imag':response_imag,
            'psd':C,
            'Eff':e_eff,
            'time':passive_dict['time'],
            'pos':passive_dict['y']
            }
    
    #%% Save dict to pkl
    
    file = open(general_path+"/combined_new.pkl", "wb")
    pickle.dump(to_save, file)
    file.close()
    print('Combine Active + Passive done')


if __name__ == '__main__':
    '''Chose the path of your experiment'''
    general_path=r'C:\Users\timo\Documents\Temp Data\bead5'
    general_path=r'Z:\till\Till\Key_experiments\AP_correctedbeta\HoxB8\2019_11_22_HoxB8_3_day3\b09'
    general_path=r'Z:\bart\Measurements\211104 on mouse oocytes\Measurement8'
    combineactivepassive(general_path)



