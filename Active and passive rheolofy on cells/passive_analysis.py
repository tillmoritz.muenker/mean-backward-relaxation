# -*- coding: utf-8 -*-
"""
Created on Wed May  8 19:54:13 2019

@author: Till

Analyse a passive experiment of multiple cycles and returns the average binned fluctuations
"""




def analysepassive(path):
    global local_vars



    import matplotlib.pyplot as plt
    import numpy as np
    import os
    import scipy as sp
    import scipy.interpolate
    import pandas as pd
    import pickle
    
    #%%
    '''Define functions'''
    def getmatfiles(path):
        #Returns all the .mat files in the given directory
        #path='Y:/till/Till/Data/2019_05_08_active_passive_075agarose_after_realignment/scan'
        filenames=np.array(os.listdir(path))                            #lists all files in dir
        filenames= np.array([i for i in filenames if ".mat" in i])      #regard only .mat files
        return(filenames)
    
    def loadData(path,number):
        import scipy.io as sio
        filenames=getmatfiles(path)
        emptyarray=np.array([])
        filepath=path+'/'+filenames[number]
        content=sio.loadmat(filepath)
        for k,v in content.items():
            if (isinstance(v,type(emptyarray)) and k!='None') :
                exec('%s=content[\'%s\']'%(k,k))                        #auto asign variable from matfile
        #print(filenames[number])
        del content
        return locals()
    
    def generatePSD(N,rate,data):
        #Generates the PSD for N samples measured with the frequency rate
        xdft=np.fft.fft(data)
        xdft=np.abs(xdft*np.conjugate(xdft))
        xdft=xdft/(N*rate)
        xdft.resize(int(N/2))
        #print(N,rate)

        # #Play around with dtft
        # factor=8
        # data_new=np.zeros(N*factor)
        # data_new[:N]=data-np.mean(data)
        
        
        # fft_new=np.fft.fft(data_new)
        # fft_new=np.abs(fft_new*np.conjugate(fft_new))
        # fft_new=fft_new/(N*rate)
        # fft_new.resize(int(N*factor/2))
        # freq_new=np.fft.fftfreq(N*factor,1/rate)[0:np.int(N*factor/2)]
        
        # freq_old=np.fft.fftfreq(N,1/rate)[0:np.int(N/2)]
        
        # indices = np.where(np.in1d(freq_new, freq_old))[0]
        # fft=fft_new[indices]
        # #print(len(fft_new))
        # #xdft=fft_new
        # print(len(xdft),len(fft_new))
        
        return(xdft)
    
    
    
    
    
    #%%
    
    
    
    
    
    #plot style
    #plt.style.use('bmh')
    
    


    
    #Choose the passive folder
    path+='/passive'
    
    '''Count number of files in folder'''
    numberoffiles=getmatfiles(path).size
    
    '''Get the files to be analysed'''
    files=getmatfiles(path)
    
    
    #Add a folder to save the figures in
    figfolder=path+'/figures/'
    try:
        os.mkdir(path+'/figures')
    except OSError:
        i=0
        #print ("Creation of the directory %s failed" % path)
    else:  
        i=0
        #print ("Successfully created the directory %s" % path)
        
        
    
    
    
    '''Create empty results arrays'''
    vars=loadData(path,0)
    N=len(vars['pos_x'][0])
    psdxx=np.zeros(np.int(N/2))
    psdyy=np.zeros(np.int(N/2))
    y_data=[]
    x_data=[]
    
    '''Load conversion factor beta'''
    constants=vars['constants'][0]
    betax=np.abs(constants[2])
    betay=np.abs(constants[3])
    
    '''First iterate over all experiments, calculate the psd and sum them up'''
    for i in range(numberoffiles):
        #open up al varibales in a given matfile
        vars=loadData(path,i)
        rate=int(vars['rate'])
        rate=2**16
        
        x=vars['pos_x'][0]/betax
        y=vars['pos_y'][0]/betay
        sumxy=vars['pos_xy'][0]
        x=x/sumxy
        y=y/sumxy
        y_data.append(y)
        x_data.append(x)
        
        # plt.figure()
        # plt.plot(x,label='x')
        # plt.plot(y,label='y')
        # plt.plot(sumxy,label='sum')
        # plt.legend()
        # plt.show()
        
        time=np.linspace(1/rate,N/rate,N)
        N=len(x)
    
        
        
        '''Now calculated the psd'''
        psdx=np.abs(generatePSD(N,rate,x))
        psdy=np.abs(generatePSD(N,rate,y))
        freq=np.fft.fftfreq(N,1/rate)[0:np.int(N/2)]
        
        print(len(psdx))
        print(len(freq))
        
    
        
        
        psdxx=psdxx+psdx
        psdyy=psdyy+psdy
        
    
        
    
    
    '''Average the PSDs by the number of repititio cycles of passive measuremnt'''
    psdxx=psdxx/numberoffiles
    psdyy=psdyy/numberoffiles
    
    import copy
    psdx_unfiltered=copy.deepcopy(psdxx)
    psdy_unfiltered=copy.deepcopy(psdyy)
    
    # plt.figure('PSD')
    # plt.loglog(freq,psdyy,label='Not binned')
    
    
    
    
    
    #%%
    '''Windowing filter- Convolution with window'''
    Window_size=4
    start=0
    stop=30
    psdxx[start:stop]=np.convolve(psdxx, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    psdyy[start:stop]=np.convolve(psdyy, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    
    Window_size=20
    start=30
    stop=100
    psdxx[start:stop]=np.convolve(psdxx, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    psdyy[start:stop]=np.convolve(psdyy, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    
    start=100
    stop=1000
    Window_size=40
    psdxx[start:stop]=np.convolve(psdxx, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    psdyy[start:stop]=np.convolve(psdyy, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    
    start=1000
    stop=10000
    Window_size=400
    psdxx[start:stop]=np.convolve(psdxx, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    psdyy[start:stop]=np.convolve(psdyy, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    
    start=10000
    stop=-1
    Window_size=4000
    psdxx[start:stop]=np.convolve(psdxx, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    psdyy[start:stop]=np.convolve(psdyy, np.ones((Window_size,))/Window_size, mode='same')[start:stop]
    
    
    
    
    
    
    
    # plt.loglog(freq,psdyy,label='binned with %f filter'%Window_size)
    # plt.legend()
    # plt.show()
    
    
    #%%
    
    
    
    '''Create array for frequenc powers'''
    power=np.linspace(0,10,11,dtype='int')
    freq_power=2**power
    passive_x=np.zeros(power.size)
    passive_y=np.zeros(power.size)
    '''Save the psd at the frequencies from 1 to 1024 HZ'''
    for i in power:

        passive_x[i]=psdxx[np.argmin(np.abs(freq-freq_power[i]))]
        passive_y[i]=psdyy[np.argmin(np.abs(freq-freq_power[i]))]
        
        #passive_x[i]=psdxx[np.argwhere(freq==freq_power[i])]
        #passive_y[i]=psdyy[np.argwhere(freq==freq_power[i])]
    
    
 
    '''Plot results and save to figure folder'''
    
    plt.figure('Particle position',figsize=(5,2))
    plt.plot(time,y,'-')
    plt.xlabel(r'Time [s]')
    plt.ylabel(r'Position [$\mu m$]')
    plt.tight_layout()
    plt.show()
    
    plt.figure('Passive x',figsize=(5,5/1.618))
    plt.title('Passive x')
    plt.loglog(freq,psdx_unfiltered,'.')
    plt.loglog(freq_power,passive_x,'o')
    plt.xlabel(r'Frequency[$Hz$]')
    plt.ylabel(r'PSD [$\mu m^2/Hz$]')
    plt.tight_layout()
    plt.show()
    plt.savefig(figfolder+'PSD x.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(figfolder+'PSD x.png',format='png',bbox_inches='tight')

    plt.figure('Passive y',figsize=(5,5/1.618))
    plt.title('Passive y')
    plt.loglog(freq,psdy_unfiltered,'.',label='PSD')
    plt.loglog(freq_power,passive_y,'o-',label='Extracted values')
    plt.xlabel(r'Frequency[Hz]')
    plt.ylabel(r'PSD [$\mu m^2/$Hz]')
    plt.tight_layout()
    plt.legend()
    plt.show()
    plt.savefig(figfolder+'PSD y.pdf',format='pdf',bbox_inches='tight')
    plt.savefig(figfolder+'PSD y.png',format='png',bbox_inches='tight')
    
    plt.figure('Response y',figsize=(5,5/1.618))
    plt.title('Response y')
    plt.loglog(freq,psdyy*freq,label='Response ')
    plt.tight_layout()
    plt.legend()
    plt.show()
    
    plt.figure('Response x',figsize=(5,5/1.618))
    plt.title('Response x')
    plt.loglog(freq,psdxx*freq,label='Response ')
    plt.legend()
    plt.tight_layout()
    plt.show()

    #%% 
    '''Now save important data in file. The format tis dictionary'''
    
    #Save Data to dict
    meta={
          'N':N,
          'rate':rate,
          'betay':betay,
          'betax':betax
          }
    to_save={
            'meta':meta,
            'time':time,
            'y':y_data,
            'x':x_data,
            'freq':freq_power,
            'psdy':passive_y,    
            'psdx':passive_x,
            
            }
    
    #%% Save dict to pkl
    
    file = open(path+"/passive_new.pkl", "wb")
    pickle.dump(to_save, file)
    file.close()
    print('Passive done')

    
    #Save passive data to csv
    # tocsv={}
    # tocsv['time [s]']=to_save['time']
    # tocsv['x [mu m]']=to_save['x'][0]
    # tocsv['y [mu m]']=to_save['y'][0]
    # to_csv=pd.DataFrame.from_dict(tocsv)
    # print(to_csv.to_csv(path+'\passive_csv.txt',index=False))
    #return locals()
    return(freq )
    


if __name__ == '__main__':
    '''Chose the path of your experiment'''
    general_path=r'Z:\till\Till\Key_experiments\AP_correctedbeta\C2C12\24_10_2019\c5b1'
    
    #locals().update(analysepassive(general_path))
    freq=analysepassive(general_path)




