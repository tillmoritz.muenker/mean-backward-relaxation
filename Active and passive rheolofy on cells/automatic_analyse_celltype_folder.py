# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 16:00:39 2020

@author: Automatic analyse experiments in celltype folder
"""

import matplotlib.pyplot as plt
import numpy as np
import os
from textwrap import wrap
import tkinter as tk
from tkinter import filedialog

import active_analysis as aa
import passive_analysis as pa
import combine_active_passive as cap 
#%%
#Plot style
plt.style.use('bmh')

#Fildedialog box
root = tk.Tk()
root.withdraw() 

'''Open experiment path, should be the directory of a celltype'''
'''Then go all the way down to the folders of single experiments'''
main_path= filedialog.askdirectory(initialdir="Y:\\till\\Till",  title='Please select a directory')
#Change dir
os.chdir(main_path)
#List all folders in dir
dir_namesN=[name for name in os.listdir(".") if os.path.isdir(name)]
#Get number of experiments in folder
N_experimentsN=np.size(dir_namesN)
for sub in dir_namesN:
    print(sub)
    sub_path=main_path+'\\'+sub
    os.chdir(sub_path)
    subdir_namesN=[name for name in os.listdir(".") if os.path.isdir(name)]
    N_experimentsN=np.size(dir_namesN)
    
    for subsub in subdir_namesN:
        print(subsub)
        subsub_path=sub_path+'\\'+subsub
        '''Now run analyse passive, analyse active and combine active,passive'''
        aa.analyseactive(subsub_path)
        pa.analysepassive(subsub_path)
        cap.combineactivepassive(subsub_path)
        
        
print('done')
    
